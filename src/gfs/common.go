package gfs

import (
	"fmt"
	"time"
)

type Path string
type ServerAddress string
type Offset int64
type ChunkIndex uint
type ChunkHandle int64
type ChunkVersion int64
type BufferHandle uint64

func (handle ChunkHandle) String() string {
	return fmt.Sprintf("%016x", uint64(handle))
}

const InvalidHandle ChunkHandle = -1

type DataBufferID struct {
	Handle    ChunkHandle
	TimeStamp uint64
}

type PathInfo struct {
	Name string

	// if it is a directory
	IsDir bool

	// if it is a file
	Length int64
	Chunks int64
}

type MutationType int

const (
	MutationWrite = iota
	MutationAppend
	MutationPad
)

type OpenMode int

const (
	OpenRead = iota
	OpenReadWrite
)

const (
	ReqReadData uint32 = iota
	ReqPushData
)

type BufferRequest struct {
	Req       uint32
	Handle    string
	Length    uint32
	ForwardTo []string
}

type ErrorCode int

const (
	Success = iota
	UnknownError
	AppendExceedChunkSize
	WriteExceedChunkSize
	ReadEOF
	NotAvailableForCopy
	PathNotExist
	FileNotExist
	FileExist
	FileSystemError
	ChunkNotFound
	NetworkError
	MutationExpired
	BufferNotFound
	DispatchError
	Timeout
	InvalidLease
	NotPrimary
	InvalidDescriptor
	FileIsDir
	FileNotDir
	ArgumentError
	DirectoryNotEmpty
	FileIsRoot
	ReadOnly
	VersionOlder
	VersionNewer
	NoReference
	InvalidReplica
	LeaseNotExpired
	ChunkNotAvailable
	ResourceLocked
	ChunkInDanger
	NoAvailableServer
	NotSecondary
	ServerDead
)

// extended error type with error code
type Error struct {
	Code ErrorCode
	Err  string
}

func (e Error) Error() string {
	return e.Err
}

// system config
const (
	LeaseExpire        = 1 * time.Minute
	LeaseExpireMin     = 5 * time.Second
	HeartbeatInterval  = 100 * time.Millisecond
	BackgroundInterval = 200 * time.Millisecond //
	ServerTimeout      = 4 * time.Second        //
	ServerCleanupTime  = 5 * time.Minute

	StabilityIncrease = 10

	//MaxChunkSize = 512 << 10 // 512KB DEBUG ONLY 64 << 20
	MaxChunkSize  = 64 << 20 // 512KB DEBUG ONLY 64 << 20
	MaxAppendSize = MaxChunkSize / 4

	DefaultNumReplicas = 3
	MinimumNumReplicas = 2

	DownloadBufferExpire = 1 * time.Minute
	DownloadBufferTick   = 10 * time.Second

	RPCTimeout = 2 * time.Minute

	HeartbeatChunkLimit = 100

	RetryInterval = 1 * time.Second //+1s

	TCPRWSize        = 64 << 10
	RPCPushDataLimit = 4 << 10
)
