package master

import (
	"fmt"
	"gfs"
	"gfs/util"
	"math/rand"
	"sync"
	"time"
)

// chunkManager manages chunks
type chunkManager struct {
	sync.RWMutex
	chunk         map[gfs.ChunkHandle]*chunkInfo
	garbage       map[gfs.ChunkHandle]bool
	needCopy      map[gfs.ChunkHandle]bool
	multiOpenLock sync.Mutex
	lm            *logManager
}

type chunkInfo struct {
	//sync.RWMutex
	mutex    *util.RWMutexEx
	handle   gfs.ChunkHandle
	location util.ArraySet // set of replica locations
	curLease lease
	version  gfs.ChunkVersion
	refCount uint

	readCount  uint
	writeCount uint

	inDanger bool
}

type lease struct {
	primary     gfs.ServerAddress
	expire      time.Time
	secondaries []gfs.ServerAddress
}

func (l *lease) isValid() bool {
	return len(l.primary) > 0 && l.expire.After(time.Now())
}

type gfsChunk struct {
	cm    *chunkManager
	cinfo *chunkInfo
	mode  gfs.OpenMode
}

func (chunk *gfsChunk) checkValid(needWrite bool) error {
	if chunk.cinfo == nil {
		return gfs.Error{Code: gfs.InvalidDescriptor, Err: "Invalid cinfo data"}
	}
	if chunk.cm == nil {
		return gfs.Error{Code: gfs.InvalidDescriptor, Err: "Invalid cm data"}
	}
	if chunk.mode != gfs.OpenRead && chunk.mode != gfs.OpenReadWrite {
		return gfs.Error{Code: gfs.InvalidDescriptor, Err: "Invalid Open Mode"}
	}
	if needWrite && chunk.mode == gfs.OpenRead {
		return gfs.Error{Code: gfs.ReadOnly, Err: fmt.Sprintf("Cannot modify chunk %v's data in read-only mode", chunk.cinfo.handle)}
	}
	return nil
}

const (
	logCMUpdateVersion = iota
	logCMAddRef
	logCMReleaseRef
	logCMCreateChunk
	logCMRemoveChunk
)

func newChunkManager(root string) *chunkManager {
	cm := &chunkManager{
		chunk:    make(map[gfs.ChunkHandle]*chunkInfo),
		garbage:  make(map[gfs.ChunkHandle]bool),
		needCopy: make(map[gfs.ChunkHandle]bool)}
	cm.lm = new(logManager)
	cm.lm.loadLog(root+"chunkmanager.log", cm.reproduceLog)
	for handle := range cm.chunk {
		cm.needCopy[handle] = true
	}
	return cm
}

func (chunk *gfsChunk) Close() {
	err := chunk.checkValid(false)
	if err != nil {
		return
	}
	if chunk.mode == gfs.OpenRead {
		chunk.cinfo.mutex.RUnlock()
	} else {
		chunk.cinfo.mutex.Unlock()
	}
	chunk.cinfo = nil
}

func (chunk *gfsChunk) GetHandle() gfs.ChunkHandle {
	err := chunk.checkValid(false)
	if err != nil {
		panic(err)
	}
	return chunk.cinfo.handle
}

func (chunk *gfsChunk) MarkInDanger() {
	err := chunk.checkValid(true)
	if err != nil {
		panic(err)
	}
	chunk.cinfo.inDanger = true
}

func (chunk *gfsChunk) UnmarkInDanger() {
	err := chunk.checkValid(true)
	if err != nil {
		panic(err)
	}
	chunk.cinfo.inDanger = true
}

func (chunk *gfsChunk) CheckInDanger() bool {
	err := chunk.checkValid(false)
	if err != nil {
		panic(err)
	}
	return chunk.cinfo.inDanger
}

// RegisterReplica adds a replica for a chunk
func (chunk *gfsChunk) RegisterReplica(addr gfs.ServerAddress) {
	err := chunk.checkValid(true)
	if err != nil {
		panic(err)
	}
	chunk.cinfo.location.Add(addr)
	if chunk.cinfo.location.Size() >= gfs.DefaultNumReplicas {
		chunk.cm.Lock()
		delete(chunk.cm.needCopy, chunk.cinfo.handle)
		chunk.cm.Unlock()
	}
}

func (chunk *gfsChunk) UnregisterReplica(addr gfs.ServerAddress) {
	err := chunk.checkValid(true)
	if err != nil {
		panic(err)
	}
	chunk.cinfo.location.Delete(addr)
	if chunk.cinfo.location.Size() < gfs.MinimumNumReplicas {
		chunk.cm.Lock()
		chunk.cm.needCopy[chunk.cinfo.handle] = true
		chunk.cm.Unlock()
	}
}

func (chunk *gfsChunk) CheckNeedCopy() {
	if chunk.cinfo.location.Size() >= gfs.DefaultNumReplicas {
		chunk.cm.Lock()
		delete(chunk.cm.needCopy, chunk.cinfo.handle)
		chunk.cm.Unlock()
	} else if chunk.cinfo.location.Size() < gfs.MinimumNumReplicas {
		chunk.cm.Lock()
		chunk.cm.needCopy[chunk.cinfo.handle] = true
		chunk.cm.Unlock()
	}
}

// GetReplicas returns the replicas of a chunk
func (chunk *gfsChunk) GetReplicas() *util.ArraySet {
	err := chunk.checkValid(false)
	if err != nil {
		panic(err)
	}
	rep := chunk.cinfo.location.GetAll()
	var ret util.ArraySet
	for _, v := range rep {
		ret.Add(v)
	}
	return &ret
}

func (chunk *gfsChunk) HasReplica(addr gfs.ServerAddress) bool {
	err := chunk.checkValid(false)
	if err != nil {
		panic(err)
	}
	return chunk.cinfo.location.Find(addr)
}

// GetLeaseHolder returns the chunkserver that hold the lease of a chunk
// (i.e. primary) and expire time of the lease. If no one has a lease,
// <del>grants one to a replica it chooses.</del>
// ^ NO WAY! Create a new lease explicitly!
// If there's no lease currently, it will return InvalidLease error
func (chunk *gfsChunk) GetLeaseHolder() (*lease, error) {
	err := chunk.checkValid(false)
	if err != nil {
		panic(err)
	}
	if !chunk.cinfo.curLease.isValid() {
		return nil, gfs.Error{Code: gfs.InvalidLease, Err: fmt.Sprintf("Chunk %v has no valid lease", chunk.cinfo.handle)}
	}
	cl := chunk.cinfo.curLease
	return &cl, nil
}

// UpdateLease updates the lease of chunk
func (chunk *gfsChunk) UpdateLease(newLease *lease) {
	err := chunk.checkValid(true)
	if err != nil {
		panic(err)
	}
	chunk.cinfo.curLease = *newLease
}

func (chunk *gfsChunk) Info() (version gfs.ChunkVersion, refcount uint) {
	err := chunk.checkValid(false)
	if err != nil {
		panic(err)
	}
	return chunk.cinfo.version, chunk.cinfo.refCount
}

func (chunk *gfsChunk) AddVersion() gfs.ChunkVersion {
	err := chunk.checkValid(true)
	if err != nil {
		panic(err)
	}
	chunk.cinfo.version++
	chunk.cm.lm.writeLog(logCMUpdateVersion, struct {
		Handle  string
		Version string
	}{Handle: fmt.Sprint(chunk.cinfo.handle), Version: fmt.Sprint(chunk.cinfo.version)})
	return chunk.cinfo.version
}

func (chunk *gfsChunk) UpdateVersion(newVersion gfs.ChunkVersion) {
	err := chunk.checkValid(true)
	if err != nil {
		panic(err)
	}
	chunk.cinfo.version = newVersion
	chunk.cm.lm.writeLog(logCMUpdateVersion, struct {
		Handle  string
		Version string
	}{Handle: fmt.Sprint(chunk.cinfo.handle), Version: fmt.Sprint(chunk.cinfo.version)})
}

func (chunk *gfsChunk) AddRef() uint {
	err := chunk.checkValid(true)
	if err != nil {
		panic(err)
	}
	if chunk.cinfo.refCount == 0 {
		chunk.cm.Lock()
		delete(chunk.cm.garbage, chunk.cinfo.handle)
		chunk.cm.Unlock()
	}
	chunk.cinfo.refCount++
	chunk.cm.lm.writeLog(logCMAddRef, fmt.Sprint(chunk.cinfo.handle))
	return chunk.cinfo.refCount
}

func (chunk *gfsChunk) ReleaseRef() uint {
	err := chunk.checkValid(true)
	if err != nil {
		panic(err)
	}
	if chunk.cinfo.refCount == 0 {
		return 0
	}
	chunk.cinfo.refCount--
	if chunk.cinfo.refCount == 0 {
		chunk.cm.Lock()
		chunk.cm.garbage[chunk.cinfo.handle] = true
		chunk.cm.Unlock()
	}
	chunk.cm.lm.writeLog(logCMReleaseRef, fmt.Sprint(chunk.cinfo.handle))
	return chunk.cinfo.refCount
}

func (cm *chunkManager) CreateChunk() gfs.ChunkHandle {
	handle := NewChunkHandle()
	cm.createChunk(handle)
	return handle
}

func (cm *chunkManager) createChunk(handle gfs.ChunkHandle) {
	cm.Lock()
	defer cm.Unlock()
	for {
		_, ok := cm.chunk[handle]
		if !ok {
			cm.chunk[handle] = &chunkInfo{
				mutex:    util.NewRWMutexEx(),
				handle:   handle,
				version:  0,
				refCount: 1}
			break
		}
		handle = NewChunkHandle()
	}
	cm.lm.writeLog(logCMCreateChunk, fmt.Sprint(handle))
}

func (cm *chunkManager) RemoveChunk(handle gfs.ChunkHandle) error {
	cm.Lock()
	defer cm.Unlock()
	_, ok := cm.chunk[handle]
	if !ok {
		return gfs.Error{Code: gfs.ChunkNotFound, Err: fmt.Sprintf("Invalid chunk handle %v", handle)}
	}
	delete(cm.chunk, handle)
	delete(cm.garbage, handle)
	cm.lm.writeLog(logCMRemoveChunk, fmt.Sprint(handle))
	return nil
}

func (cm *chunkManager) GetGarbages() (garbage []gfs.ChunkHandle) {
	cm.RLock()
	defer cm.RUnlock()
	for k, v := range cm.garbage {
		if !v {
			continue
		}
		garbage = append(garbage, k)
	}
	return
}

func (cm *chunkManager) GetChunksNeedCopy() (chunks []gfs.ChunkHandle) {
	cm.RLock()
	defer cm.RUnlock()
	for k, v := range cm.needCopy {
		if v {
			chunks = append(chunks, k)
		}
	}
	return
}

func (cm *chunkManager) OpenChunk(handle gfs.ChunkHandle, mode gfs.OpenMode) (*gfsChunk, error) {
	cm.RLock()
	cinfo, ok := cm.chunk[handle]
	cm.RUnlock()
	if !ok {
		return nil, gfs.Error{Code: gfs.ChunkNotFound, Err: fmt.Sprintf("Invalid chunk handle %v", handle)}
	}
	switch mode {
	case gfs.OpenRead:
		cinfo.mutex.RLock()
	case gfs.OpenReadWrite:
		cinfo.mutex.Lock()
	default:
		return nil, gfs.Error{Code: gfs.ArgumentError, Err: "Invalid open mode"}
	}
	return &gfsChunk{cm: cm, cinfo: cinfo, mode: mode}, nil
}

func (cm *chunkManager) TryOpenChunk(handle gfs.ChunkHandle, mode gfs.OpenMode) (*gfsChunk, error) {
	cm.RLock()
	cinfo, ok := cm.chunk[handle]
	cm.RUnlock()
	if !ok {
		return nil, gfs.Error{Code: gfs.ChunkNotFound, Err: fmt.Sprintf("Invalid chunk handle %v", handle)}
	}
	if mode != gfs.OpenRead && mode != gfs.OpenReadWrite {
		return nil, gfs.Error{Code: gfs.ArgumentError, Err: "Invalid open mode"}
	} else if mode == gfs.OpenRead && !cinfo.mutex.TryRLock() {
		return nil, gfs.Error{Code: gfs.ResourceLocked, Err: fmt.Sprintf("Chunk %v's mutex is locked", handle)}
	} else if mode == gfs.OpenReadWrite && !cinfo.mutex.TryLock() {
		return nil, gfs.Error{Code: gfs.ResourceLocked, Err: fmt.Sprintf("Chunk %v's mutex is locked", handle)}
	}
	return &gfsChunk{cm: cm, cinfo: cinfo, mode: mode}, nil
}

func (cm *chunkManager) OpenChunks(handles []gfs.ChunkHandle, mode gfs.OpenMode) (chunks []*gfsChunk, errors []error) {
	cm.multiOpenLock.Lock()
	defer cm.multiOpenLock.Unlock()
	chunks, errors = make([]*gfsChunk, len(handles)), make([]error, len(handles))
	for idx := 0; idx < len(handles); idx++ {
		chunks[idx], errors[idx] = cm.OpenChunk(handles[idx], mode)
	}
	return
}

func (cm *chunkManager) reproduceLog(logItem logType) error {
	var handle uint64
	switch logItem.LogType {
	case logCMCreateChunk:
		fmt.Sscanf(logItem.Data.(string), "%016x", &handle)
		cm.createChunk(gfs.ChunkHandle(handle))
	case logCMRemoveChunk:
		fmt.Sscanf(logItem.Data.(string), "%016x", &handle)
		err := cm.RemoveChunk(gfs.ChunkHandle(handle))
		if err != nil {
			return err
		}
	case logCMUpdateVersion:
		param := logItem.Data.(map[string]interface{})
		fmt.Sscanf(param["Handle"].(string), "%016x", &handle)
		var version int64
		fmt.Sscanf(param["Version"].(string), "%d", &version)
		chunk, err := cm.OpenChunk(gfs.ChunkHandle(handle), gfs.OpenReadWrite)
		if err != nil {
			return err
		}
		chunk.UpdateVersion(gfs.ChunkVersion(version))
		chunk.Close()
	case logCMAddRef:
		fmt.Sscanf(logItem.Data.(string), "%016x", &handle)
		chunk, err := cm.OpenChunk(gfs.ChunkHandle(handle), gfs.OpenReadWrite)
		if err != nil {
			return err
		}
		chunk.AddRef()
		chunk.Close()
	case logCMReleaseRef:
		fmt.Sscanf(logItem.Data.(string), "%016x", &handle)
		chunk, err := cm.OpenChunk(gfs.ChunkHandle(handle), gfs.OpenReadWrite)
		if err != nil {
			return err
		}
		chunk.ReleaseRef()
		chunk.Close()
	}
	return nil
}

//NewChunkHandle generate a unique chunk handle for a new chunk
func NewChunkHandle() gfs.ChunkHandle {
	return gfs.ChunkHandle((uint64(time.Now().Nanosecond()) << 32) | uint64(rand.Uint32()))
}
