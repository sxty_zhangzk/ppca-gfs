package master

import (
	"fmt"
	"net"
	"net/rpc"
	"time"

	log "github.com/Sirupsen/logrus"

	"gfs"
	"gfs/util"
)

// Master Server struct
type Master struct {
	address    gfs.ServerAddress // master server address
	serverRoot string            // path to metadata storage
	l          net.Listener
	shutdown   chan struct{}

	dead bool

	nm  *namespaceManager
	cm  *chunkManager
	csm *chunkServerManager
}

type GFSLogFormatter struct{}

//Format your mother fucker
func (fm *GFSLogFormatter) Format(e *log.Entry) ([]byte, error) {
	return []byte(fmt.Sprintf("[%02d:%02d:%02d.%02d|%v] %s\n",
		e.Time.Hour(), e.Time.Minute(), e.Time.Second(), e.Time.Nanosecond()/10000000,
		e.Level, e.Message)), nil
}

// NewAndServe starts a master and returns the pointer to it.
func NewAndServe(address gfs.ServerAddress, serverRoot string) *Master {
	m := &Master{
		address:    address,
		serverRoot: serverRoot,
		shutdown:   make(chan struct{}),
	}
	if ch := m.serverRoot[len(m.serverRoot)-1]; ch != '/' && ch != '\\' {
		m.serverRoot += "/"
	}

	//log.SetLevel(log.DebugLevel)
	//log.SetFormatter(&GFSLogFormatter{})

	m.nm = newNamespaceManager(m.serverRoot)
	m.cm = newChunkManager(m.serverRoot)
	m.csm = newChunkServerManager(m.cm)

	rpcs := rpc.NewServer()
	rpcs.Register(m)
	l, e := net.Listen("tcp", string(m.address))
	if e != nil {
		log.Fatal("listen error:", e)
		log.Exit(1)
	}
	m.l = l

	// RPC Handler
	go func() {
		for {
			select {
			case <-m.shutdown:
				return
			default:
			}
			conn, err := m.l.Accept()
			if err == nil {
				go func() {
					rpcs.ServeConn(conn)
					conn.Close()
				}()
			} else {
				if !m.dead {
					log.Fatal("accept error:", err)
					log.Exit(1)
				}
			}
		}
	}()

	// Background Task
	go func() {
		ticker := time.Tick(gfs.BackgroundInterval)
		for {
			select {
			case <-m.shutdown:
				return
			default:
			}
			<-ticker

			err := m.BackgroundActivity()
			if err != nil {
				log.Fatal("Background error ", err)
			}
		}
	}()

	log.Infof("Master is running now. addr = %v, root path = %v", address, serverRoot)

	return m
}

// Shutdown shuts down master
func (m *Master) Shutdown() {
	if !m.dead {
		log.Infof("Master shuts down")
		m.dead = true
		m.l.Close()
		close(m.shutdown)
	}
}

// BackgroundActivity does all the background activities:
// dead chunkserver handling, garbage collection, stale replica detection, etc
func (m *Master) BackgroundActivity() (err error) {
	errlist := ""
	go func() {
		if errlist != "" {
			err = fmt.Errorf(errlist)
		} else {
			err = nil
		}
	}()
	//dead chunkserver handle
	m.csm.DetectDeadServers()
	//
	//garbage collection
	garbage := m.cm.GetGarbages()
	for _, handle := range garbage {
		m.cm.RemoveChunk(handle)
	}
	//naive
	//stale replica detection
	//in heartbeat
	//
	chunksNeedCopy := m.cm.GetChunksNeedCopy()
	for _, handle := range chunksNeedCopy {
		m.csm.ReReplication(handle)
	}

	m.csm.UnregisterFailedChunks()

	return
}

// RPCHeartbeat is called by chunkserver to let the master know that a chunkserver is alive.
// Lease extension request is included.
func (m *Master) RPCHeartbeat(args gfs.HeartbeatArg, reply *gfs.HeartbeatReply) (rpcerr error) {
	//log.Debugf("\033[35;1m[Heartbeat|%s] LeaseExtensions: %+v, Chunks: %+v\033[m", args.Address, args.LeaseExtensions, args.Chunks)
	//defer func() { log.Debugf("\033[35m[Heartbeat|%s] Garbages: %+v\033[m", args.Address, reply.Garbages) }()

	//log.Debugf("## %s Heartbeat 1", args.Address)

	defer util.SerializeError(&rpcerr)
	m.csm.Heartbeat(args.Address)

	//log.Debugf("## %s Heartbeat 2", args.Address)

	for _, v := range args.Chunks {
		chunk, err := m.cm.TryOpenChunk(v.Handle, gfs.OpenReadWrite)
		if err != nil && err.(gfs.Error).Code == gfs.ResourceLocked {
			continue
		} else if err != nil {
			reply.Garbages = append(reply.Garbages, v.Handle)
		} else {
			if ver, _ := chunk.Info(); ver != v.Version {
				reply.Garbages = append(reply.Garbages, v.Handle)
				if err != nil {
					continue
				}
				m.csm.UnregisterReplica(args.Address, v.Handle)
				chunk.UnregisterReplica(args.Address)
			} else {
				if !chunk.GetReplicas().Find(args.Address) {
					if err != nil {
						continue
					}
					if ver, _ := chunk.Info(); ver == v.Version && !chunk.GetReplicas().Find(args.Address) {
						m.csm.RegisterReplica(args.Address, chunk)
					}
				}
			}
			chunk.Close()
		}
	}

	//log.Debugf("## %s Heartbeat 3", args.Address)

	for _, v := range args.FailedChunks {
		chunk, err := m.cm.TryOpenChunk(v, gfs.OpenReadWrite)
		if err != nil && err.(gfs.Error).Code == gfs.ResourceLocked {
			m.csm.AddFailedChunk(v, args.Address)
			continue
		} else if err != nil {
			continue
		}
		m.csm.UnregisterReplica(args.Address, v)
		chunk.UnregisterReplica(args.Address)
		chunk.Close()
		reply.Garbages = append(reply.Garbages, v)
	}

	//log.Debugf("## %s Heartbeat 4", args.Address)

	for _, v := range m.csm.GetGarbageChunks(args.Address) {
		reply.Garbages = append(reply.Garbages, v)
	}

	//zzk := make([]bool, len(args.LeaseExtensions))
	if len(args.LeaseExtensions) > 0 {
		go m.csm.GrantLeasesByHandle(args.LeaseExtensions, args.Address, true)
	}

	//log.Debugf("## %s Heartbeat 5", args.Address)
	/*errs, _ := m.csm.GrantLeases(args.LeaseExtensions, args.Address)
	for i,j:=range errs{
		if j==nil{
			chunk,e:=m.cm.OpenChunk(args.LeaseExtensions[i],gfs.OpenReadWrite)
			if e!=nil{
				continue
			}
			//chunk.UpdateLease(&lease{primary: args.primary, expire: time.Now().Add(gfs.LeaseExpire), secondaries: l.secondaries})
		}
	}*/
	return nil
}

// RPCGetPrimaryAndSecondaries returns lease holder and secondaries of a chunk.
// If no one holds the lease currently, grant one.
func (m *Master) RPCGetPrimaryAndSecondaries(args gfs.GetPrimaryAndSecondariesArg, reply *gfs.GetPrimaryAndSecondariesReply) (rpcerr error) {
	log.Debugf("RPCCall: GetPrimaryAndSecondaries, args=%v", args)
	defer func() { log.Debugf("RPCReturn: GetPrimaryAndSecondaries, reply=%v, error=%v", *reply, rpcerr) }()

	defer util.SerializeError(&rpcerr)
	//m.cm.Lock()
	//defer m.cm.Unlock()
	chunk, err := m.cm.OpenChunk(args.Handle, gfs.OpenReadWrite)
	if err != nil {
		return err
	}
	defer chunk.Close()
	temp, err := chunk.GetLeaseHolder()
	if err != nil && err.(gfs.Error).Code == gfs.InvalidLease {
		replicas := chunk.GetReplicas().GetAll()
		temp = nil
		for _, v := range replicas {
			primary := v.(gfs.ServerAddress)
			leaLength, err := m.csm.GetLeaseLength(primary)
			if err == nil {
				newLease, elist, err := m.csm.GrantLeases([]*gfsChunk{chunk}, primary, leaLength)
				if err == nil && (elist[0] == nil || elist[0].(gfs.Error).Code == gfs.LeaseNotExpired) {
					temp = newLease[0]
					break
				}
			}
		}
		if temp == nil {
			return gfs.Error{Code: gfs.ChunkNotAvailable, Err: fmt.Sprintf("No available primary for chunk %v", args.Handle)}
		}
	} else if err != nil {
		return err
	}
	*reply = gfs.GetPrimaryAndSecondariesReply{Primary: temp.primary, Expire: temp.expire, Secondaries: temp.secondaries}
	return nil
}

// RPCGetReplicas is called by client to find all chunkservers that hold the chunk.
func (m *Master) RPCGetReplicas(args gfs.GetReplicasArg, reply *gfs.GetReplicasReply) (rpcerr error) {
	log.Debugf("RPCCall: GetRepicas, args=%v", args)
	defer func() { log.Debugf("RPCReturn: GetReplicas, reply=%v, error=%v", *reply, rpcerr) }()

	defer util.SerializeError(&rpcerr)
	//m.cm.Lock()
	//defer m.cm.Unlock()
	chunk, err := m.cm.OpenChunk(args.Handle, gfs.OpenRead)
	if err != nil {
		return err
	}
	defer chunk.Close()
	temp := chunk.GetReplicas()
	//reply.Locations = []gfs.ServerAddress(temp.GetAll())
	addrs := temp.GetAll()
	for _, v := range addrs {
		reply.Locations = append(reply.Locations, v.(gfs.ServerAddress))
	}
	return nil
}

// RPCCreateFile is called by client to create a new file
func (m *Master) RPCCreateFile(args gfs.CreateFileArg, reply *gfs.CreateFileReply) (rpcerr error) {
	log.Debugf("RPCCall: CreateFile, args=%v", args)
	defer func() { log.Debugf("RPCReturn: CreateFile, reply=%v, error=%v", *reply, rpcerr) }()

	defer util.SerializeError(&rpcerr)
	var e error
	e = m.nm.Create(args.Path)
	if e != nil {
		return e
	}
	return
}

// RPCMkdir is called by client to make a new directory
func (m *Master) RPCMkdir(args gfs.MkdirArg, reply *gfs.MkdirReply) (rpcerr error) {
	log.Debugf("RPCCall: Mkdir, args=%v", args)
	defer func() { log.Debugf("RPCReturn: Mkdir, reply=%v, error=%v", *reply, rpcerr) }()

	defer util.SerializeError(&rpcerr)
	return m.nm.Mkdir(args.Path)
}

// RPCGetFileInfo is called by client to get file information
func (m *Master) RPCGetFileInfo(args gfs.GetFileInfoArg, reply *gfs.GetFileInfoReply) (rpcerr error) {
	defer util.SerializeError(&rpcerr)
	File, err := m.nm.OpenFile(args.Path, gfs.OpenRead)
	if err != nil {
		return err
	}
	defer File.Close()
	temp := File.Stat()
	reply.Chunks, reply.IsDir, reply.Length = temp.Chunks, temp.IsDir, 0
	return
}

// RPCList is called by client to get the file list
func (m *Master) RPCList(args gfs.ListArg, reply *gfs.ListReply) (rpcerr error) {
	log.Debugf("RPCCall: List, args=%v", args)
	defer func() { log.Debugf("RPCReturn: List, reply=%v, error=%v", *reply, rpcerr) }()

	defer util.SerializeError(&rpcerr)
	File, err := m.nm.OpenFile(args.Path, gfs.OpenRead)
	if err != nil {
		return err
	}
	defer File.Close()
	reply.Files, err = File.List()
	return
}

// RPCGetChunkHandle returns the chunk handle of (path, index).
// If the requested index is bigger than the number of chunks of this path by exactly one, create one.
func (m *Master) RPCGetChunkHandle(args gfs.GetChunkHandleArg, reply *gfs.GetChunkHandleReply) (rpcerr error) {
	log.Debugf("RPCCall: GetChunkHandle, args=%v", args)
	defer func() { log.Debugf("RPCReturn: GetChunkHandle, reply=%v, error=%v", *reply, rpcerr) }()

	defer util.SerializeError(&rpcerr)
	File, err := m.nm.OpenFile(args.Path, gfs.OpenRead)
	if err != nil {
		return err
	}
	reply.Handle, err = File.GetChunk(args.Index)
	File.Close()
	if err != nil {
		if err.(gfs.Error).Code != gfs.ChunkNotFound {
			return err
		}
		File, err = m.nm.OpenFile(args.Path, gfs.OpenReadWrite)
		if err != nil {
			return err
		}
		defer File.Close()
		reply.Handle, err = File.GetChunk(args.Index)
		if err != nil && err.(gfs.Error).Code == gfs.ChunkNotFound {
			if File.Stat().Chunks != int64(args.Index) {
				return gfs.Error{Code: gfs.ChunkNotFound,
					Err: fmt.Sprintf("Cannot get chunk %d's handle of file '%s', which has %d chunks", args.Index, args.Path, File.Stat().Chunks)}
			}
			servers, err := m.csm.ChooseServers(gfs.DefaultNumReplicas)
			if err != nil {
				return err
			}
			log.Debugf("Choosed servers: %v", servers)
			handle := m.cm.CreateChunk()
			log.Debugf("Ready to AddChunk: servers(%v) handle(%v)", servers, handle)
			err = m.csm.AddChunk(servers, handle)
			if err != nil {
				m.cm.RemoveChunk(handle)
				return err
			}
			log.Debugf("Ready to AddChunk in file: handle(%v)", handle)
			File.AddChunk(handle)
			reply.Handle = handle
		} else if err != nil {
			return err
		}
	}
	return nil
}

func (m *Master) RPCGetTime(args gfs.GetTimeArg, reply *gfs.GetTimeReply) error {
	reply.Time = time.Now()
	return nil
}
