package master

import (
	"bytes"
	"encoding/json"
	"io"
	"os"
	"sync"

	log "github.com/Sirupsen/logrus"
)

type logManager struct {
	sync.Mutex
	file *os.File
	flag bool
}

type logType struct {
	LogType int
	Data    interface{}
}

func (lm *logManager) writeLog(typeID int, data interface{}) {
	if lm.file == nil || !lm.flag {
		return
	}
	lm.Lock()
	defer lm.Unlock()
	buffer := new(bytes.Buffer)
	enc := json.NewEncoder(buffer)
	enc.Encode(logType{LogType: typeID, Data: data})
	lm.file.Write(buffer.Bytes())
}

func (lm *logManager) loadLog(filepath string, funcReproduce func(logType) error) {
	flog, err := os.OpenFile(filepath, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		log.Errorf("Failed to open namespace log file: %v", err)
		return
	}
	lm.file = flog
	lm.flag = false

	dec := json.NewDecoder(lm.file)
	for {
		var logItem logType
		err := dec.Decode(&logItem)
		if err == io.EOF {
			break
		} else if err != nil {
			log.Errorf("Failed to decode log file: %v", err)
			break
		}
		err = funcReproduce(logItem)
		if err != nil {
			log.Errorf("Failed to reproduce log: %v", err)
		}
	}

	lm.flag = true
}
