package master

import (
	"fmt"
	"gfs"
	"strings"
	"sync"
)

type namespaceManager struct {
	root *nsTree
	lm   *logManager
}

type nsTree struct {
	sync.RWMutex

	path     gfs.Path
	fileName string
	parent   *nsTree
	// if it is a directory
	isDir            bool
	children         map[string]*nsTree
	childrenMapMutex sync.RWMutex //mutex for `children'

	// if it is a file
	chunks []gfs.ChunkHandle
}

type gfsFile struct {
	ns   *nsTree
	nm   *namespaceManager
	mode gfs.OpenMode
}

const (
	logNMCreateFile = iota
	logNMMkdir
	logNMAddChunk
	logNMDelete
)

func (file *gfsFile) checkValid(needWrite bool) error {
	if file.ns == nil {
		return gfs.Error{Code: gfs.InvalidDescriptor, Err: "Invalid gfsFile struct"}
	}
	if file.mode != gfs.OpenRead && file.mode != gfs.OpenReadWrite {
		return gfs.Error{Code: gfs.InvalidDescriptor, Err: "Invalid open mode"}
	}
	if needWrite && file.mode == gfs.OpenRead {
		return gfs.Error{Code: gfs.ReadOnly, Err: fmt.Sprintf("Cannot write to file '%s' which is opened as read-only mode", file.ns.fileName)}
	}
	return nil
}

func (file *gfsFile) IsDir() bool {
	err := file.checkValid(false)
	if err != nil {
		panic(err)
	}
	return file.ns.isDir
}

func (file *gfsFile) List() (fileinfo []gfs.PathInfo, err error) {
	err = file.checkValid(false)
	if err != nil {
		panic(err)
	}
	if !file.ns.isDir {
		return nil, gfs.Error{Code: gfs.FileNotDir, Err: fmt.Sprintf("File '%s' is not a directory", file.ns.fileName)}
	}
	file.ns.childrenMapMutex.RLock()
	for _, v := range file.ns.children {
		v.RLock()
		fileinfo = append(fileinfo, gfs.PathInfo{
			Name:   v.fileName,
			IsDir:  v.isDir,
			Chunks: int64(len(v.chunks))})
		v.RUnlock()
	}
	file.ns.childrenMapMutex.RUnlock()
	return
}

func (file *gfsFile) Stat() gfs.PathInfo {
	err := file.checkValid(false)
	if err != nil {
		panic(err)
	}
	return gfs.PathInfo{
		Name:   file.ns.fileName,
		IsDir:  file.ns.isDir,
		Chunks: int64(len(file.ns.chunks))}
}

func (file *gfsFile) AddChunk(handle gfs.ChunkHandle) {
	err := file.checkValid(true)
	if err != nil {
		panic(err)
	}
	file.ns.chunks = append(file.ns.chunks, handle)
	file.nm.lm.writeLog(logNMAddChunk, struct {
		Path   string
		Handle string
	}{Path: string(file.ns.path), Handle: fmt.Sprint(handle)})
}

func (file *gfsFile) GetChunk(idx gfs.ChunkIndex) (gfs.ChunkHandle, error) {
	err := file.checkValid(false)
	if err != nil {
		panic(err)
	}
	if idx >= gfs.ChunkIndex(len(file.ns.chunks)) {
		return gfs.InvalidHandle, gfs.Error{Code: gfs.ChunkNotFound, Err: fmt.Sprintf("Try to get chunk %d of '%s', but it only have %d chunks", idx, file.ns.fileName, len(file.ns.chunks))}
	}
	return file.ns.chunks[idx], nil
}

func (file *gfsFile) Delete() error {
	err := file.checkValid(true)
	if err != nil {
		panic(err)
	}
	if file.ns.parent == nil {
		return gfs.Error{Code: gfs.FileIsRoot, Err: "Try to delete root"}
	}
	file.ns.parent.childrenMapMutex.Lock()
	delete(file.ns.parent.children, file.ns.fileName)
	file.ns.parent.childrenMapMutex.Unlock()
	file.Close()
	file.nm.lm.writeLog(logNMDelete, file.ns.path)
	return nil
}

func (file *gfsFile) Close() {
	err := file.checkValid(false)
	if err != nil {
		return
	}
	if file.mode == gfs.OpenRead {
		file.ns.RUnlock()
	} else {
		file.ns.Unlock()
	}
	for i := file.ns.parent; i != nil; i = i.parent {
		i.RUnlock()
	}
	file.ns = nil
}

func newNamespaceManager(root string) (nm *namespaceManager) {
	nm = &namespaceManager{
		root: &nsTree{
			isDir:    true,
			children: make(map[string]*nsTree),
			path:     gfs.Path("/")}}
	nm.lm = new(logManager)
	nm.lm.loadLog(root+"namespace.log", nm.reproduceLog)
	return
}

// Create creates an empty file on path p. All parents should exist.
func (nm *namespaceManager) Create(p gfs.Path) error {
	err := nm.createFile(p, &nsTree{
		isDir:    false,
		children: make(map[string]*nsTree)})
	if err != nil {
		return err
	}
	nm.lm.writeLog(logNMCreateFile, p)
	return nil
}

// Mkdir creates a directory on path p. All parents should exist.
func (nm *namespaceManager) Mkdir(p gfs.Path) error {
	err := nm.createFile(p, &nsTree{
		isDir:    true,
		children: make(map[string]*nsTree)})
	if err != nil {
		return err
	}
	nm.lm.writeLog(logNMMkdir, p)
	return nil
}

func (nm *namespaceManager) OpenFile(p gfs.Path, mode gfs.OpenMode) (retfile *gfsFile, reterr error) {
	if len(p) == 0 || p[0] != '/' {
		return nil, gfs.Error{Code: gfs.PathNotExist, Err: "Invalid Path"}
	}
	if mode != gfs.OpenRead && mode != gfs.OpenReadWrite {
		return nil, gfs.Error{Code: gfs.ArgumentError, Err: "Invalid open mode"}
	}

	var stackUnlock []*nsTree
	defer func() {
		if reterr == nil {
			return
		}
		for i := len(stackUnlock) - 1; i >= 0; i-- {
			stackUnlock[i].RUnlock()
		}
	}()

	vPath := strings.FieldsFunc(string(p), func(r rune) bool { return r == '/' })
	now := nm.root
	now.RLock()
	stackUnlock = append(stackUnlock, now)
	if len(vPath) == 0 {
		if mode == gfs.OpenReadWrite {
			return nil, gfs.Error{Code: gfs.ArgumentError, Err: "Cannot open root as read-write mode"}
		}
		return &gfsFile{ns: now, mode: mode}, nil
	}
	for _, v := range vPath[:len(vPath)-1] {
		if len(v) == 0 {
			continue
		}
		tmp := now
		tmp.childrenMapMutex.RLock()
		var ok bool
		now, ok = now.children[v]
		tmp.childrenMapMutex.RUnlock()
		now.RLock()
		stackUnlock = append(stackUnlock, now)
		if !ok {
			return nil, gfs.Error{Code: gfs.PathNotExist, Err: "Path '" + string(p) + "' doesn't exist"}
		}
	}
	tmp := now
	tmp.childrenMapMutex.RLock()
	now, ok := now.children[vPath[len(vPath)-1]]
	tmp.childrenMapMutex.RUnlock()
	if !ok {
		return nil, gfs.Error{Code: gfs.PathNotExist, Err: "Path '" + string(p) + "' doesn't exist"}
	}

	if mode == gfs.OpenRead {
		now.RLock()
	} else {
		now.Lock()
	}
	return &gfsFile{ns: now, nm: nm, mode: mode}, nil
}

func (nm *namespaceManager) createFile(p gfs.Path, ns *nsTree) error {
	if len(p) == 0 || p[0] != '/' {
		return gfs.Error{Code: gfs.PathNotExist, Err: "Invalid Path"}
	}
	vPath := strings.FieldsFunc(string(p), func(r rune) bool { return r == '/' })
	if len(vPath) == 0 {
		return gfs.Error{Code: gfs.ArgumentError, Err: "Cannot create file as root"}
	}

	now := nm.root
	now.RLock()
	defer now.RUnlock()
	for _, v := range vPath[:len(vPath)-1] {
		if len(v) == 0 {
			continue
		}
		var ok bool
		now, ok = now.children[v]
		now.RLock()
		defer now.RUnlock()
		if !ok {
			return gfs.Error{Code: gfs.PathNotExist, Err: "Path '" + string(p) + "' doesn't exist"}
		}
	}
	fname := vPath[len(vPath)-1]
	if _, ok := now.children[fname]; ok {
		return gfs.Error{Code: gfs.FileExist, Err: "File '" + fname + "' has existed"}
	}
	ns.fileName = fname
	ns.parent = now
	ns.path = p
	now.childrenMapMutex.Lock()
	now.children[fname] = ns
	now.childrenMapMutex.Unlock()
	return nil
}

func (nm *namespaceManager) reproduceLog(logItem logType) error {
	switch logItem.LogType {
	case logNMCreateFile:
		nm.Create(gfs.Path(logItem.Data.(string)))
	case logNMMkdir:
		nm.Mkdir(gfs.Path(logItem.Data.(string)))
	case logNMDelete:
		file, err := nm.OpenFile(gfs.Path(logItem.Data.(string)), gfs.OpenReadWrite)
		if err != nil {
			return err
		}
		err = file.Delete()
		if err != nil {
			return err
		}
		file.Close()
	case logNMAddChunk:
		param := logItem.Data.(map[string]interface{})
		file, err := nm.OpenFile(gfs.Path(param["Path"].(string)), gfs.OpenReadWrite)
		if err != nil {
			return err
		}
		var handle uint64
		fmt.Sscanf(param["Handle"].(string), "%016x", &handle)
		file.AddChunk(gfs.ChunkHandle(handle))
		file.Close()
	}
	return nil
}
