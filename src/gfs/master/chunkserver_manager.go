package master

import (
	"fmt"
	"gfs"
	"gfs/util"
	"sync"
	"sync/atomic"
	"time"

	log "github.com/Sirupsen/logrus"
)

// chunkServerManager manages chunkservers
type chunkServerManager struct {
	sync.RWMutex
	servers map[gfs.ServerAddress]*chunkServerInfo
	cm      *chunkManager
}

func newChunkServerManager(cm *chunkManager) *chunkServerManager {
	csm := &chunkServerManager{
		servers: make(map[gfs.ServerAddress]*chunkServerInfo),
		cm:      cm}
	return csm
}

type chunkServerInfo struct {
	sync.RWMutex
	addr          gfs.ServerAddress
	lastHeartbeat time.Time
	chunks        map[gfs.ChunkHandle]bool // set of chunks that the chunkserver has
	failedChunks  util.ArraySet
	garbageChunks util.ArraySet

	deadlock  *util.RWMutexEx
	dead      int32
	stability int32
}

// Hearbeat marks the chunkserver alive for now.
func (csm *chunkServerManager) Heartbeat(addr gfs.ServerAddress) {
	csm.RLock()
	csInfo, ok := csm.servers[addr]
	csm.RUnlock()
	if !ok {
		csm.Lock()
		csInfo, ok = csm.servers[addr]
		if !ok {
			csInfo = &chunkServerInfo{
				addr:          addr,
				chunks:        make(map[gfs.ChunkHandle]bool),
				lastHeartbeat: time.Now(),
				dead:          1,
				stability:     10000,
				deadlock:      util.NewRWMutexEx()}
			csm.servers[addr] = csInfo
		}
		csm.Unlock()
	}
	if atomic.LoadInt32(&csInfo.dead) == 1 {
		var reply gfs.GetChunkListReply
		err := util.Call(addr, "ChunkServer.RPCGetChunkList", gfs.GetChunkListArg{}, &reply)
		if err == nil {
			go func() {
				for _, v := range reply.Chunks {
					chunk, err := csm.cm.OpenChunk(v.Handle, gfs.OpenReadWrite)
					if err != nil {
						continue

					}
					if ver, _ := chunk.Info(); ver == v.Version {
						chunk.RegisterReplica(addr)
						chunk.Close()
						csInfo.Lock()
						csInfo.chunks[v.Handle] = true
						csInfo.Unlock()
					} else {
						chunk.Close()
					}
				}
			}()
		}
	}
	csInfo.Lock()
	atomic.StoreInt32(&csInfo.dead, 0)
	csInfo.lastHeartbeat = time.Now()
	if csInfo.stability+gfs.StabilityIncrease > 10000 {
		atomic.StoreInt32(&csInfo.stability, 10000)
	} else {
		atomic.AddInt32(&csInfo.stability, gfs.StabilityIncrease)
	}
	csInfo.Unlock()
	//log.Debugf("[Heartbeat|%s] Time: %02d:%02d", addr, csInfo.lastHeartbeat.Minute(), csInfo.lastHeartbeat.Second())
	return
}

func (csm *chunkServerManager) RegisterReplica(addr gfs.ServerAddress, chunk *gfsChunk) {
	csm.RLock()
	csInfo, ok := csm.servers[addr]
	csm.RUnlock()
	if !ok {
		return
	}
	csInfo.Lock()
	if atomic.LoadInt32(&csInfo.dead) == 0 {
		chunk.RegisterReplica(addr)
		csInfo.chunks[chunk.GetHandle()] = true
	}
	csInfo.Unlock()
}

func (csm *chunkServerManager) UnregisterReplica(addr gfs.ServerAddress, handle gfs.ChunkHandle) {
	csm.RLock()
	csInfo, ok := csm.servers[addr]
	csm.RUnlock()
	if !ok {
		return
	}
	csInfo.Lock()
	delete(csInfo.chunks, handle)
	csInfo.Unlock()
}

// AddChunk creates a chunk on given chunkservers
func (csm *chunkServerManager) AddChunk(addrs []gfs.ServerAddress, handle gfs.ChunkHandle) (reterr error) {
	log.Debugf("Call: AddChunk addrs: %v, handle: %v", addrs, handle)
	defer log.Debugf("Return: AddChunks error: %v", reterr)

	chunk, err := csm.cm.OpenChunk(handle, gfs.OpenReadWrite)
	if err != nil {
		return err
	}
	defer chunk.Close()
	defer chunk.CheckNeedCopy()
	errlist := ""
	errCount := 0
	for _, addr := range addrs {
		csm.RLock()
		csInfo, ok := csm.servers[addr]
		csm.RUnlock()
		if !ok {
			errlist += "Unknown Server Address!;"
			continue
		}
		if atomic.LoadInt32(&csInfo.dead) == 1 {
			continue
		}

		var reply gfs.CreateChunkReply
		var err gfs.Error
		//rpcerr := util.Call(addr, "ChunkServer.RPCCreateChunk", gfs.CreateChunkArg{Handle: handle}, &reply)
		rpcerr := csm.CallChunkServer(csInfo, "ChunkServer.RPCCreateChunk", gfs.CreateChunkArg{Handle: handle}, &reply)
		if rpcerr != nil {
			err.Err = rpcerr.Error()
			errCount++
		} else {
			csInfo.Lock()
			if atomic.LoadInt32(&csInfo.dead) == 0 {
				chunk.RegisterReplica(addr)
				csInfo.chunks[handle] = true
			}
			csInfo.Unlock()
		}
		if err.Err != "" {
			errlist += err.Error() + ";"
		}
	}
	if errCount == len(addrs) {
		return fmt.Errorf(errlist)
	}
	return nil
}

// ChooseReReplication chooses servers to perform re-replication
// called when the replicas number of a chunk is less than gfs.MinimumNumReplicas
// returns two server address, the master will call 'from' to send a copy to 'to'
func (csm *chunkServerManager) ChooseReReplication(handle gfs.ChunkHandle) (from, to gfs.ServerAddress, reterr error) {
	chunk, err := csm.cm.OpenChunk(handle, gfs.OpenRead)
	if err != nil {
		reterr = err
		return
	}
	defer chunk.Close()
	replicas := chunk.GetReplicas()
	if replicas.Size() == 0 {
		reterr = gfs.Error{Code: gfs.NotAvailableForCopy, Err: "No available source server"}
		return
	}
	csm.RLock()
	for k, v := range csm.servers {
		if atomic.LoadInt32(&v.dead) == 1 {
			continue
		}
		if !replicas.Find(k) {
			to = k
			break
		}
	}
	csm.RUnlock()
	if len(to) == 0 {
		reterr = gfs.Error{Code: gfs.NotAvailableForCopy, Err: "No available destination server"}
		return
	}
	from = replicas.RandomPick().(gfs.ServerAddress)
	return
}

func (csm *chunkServerManager) ReReplication(handle gfs.ChunkHandle) (reterr error) {
	from, to, err := csm.ChooseReReplication(handle)
	if err != nil {
		return err
	}
	var reply gfs.SendCopyReply
	/*err = util.Call(from, "ChunkServer.RPCSendCopy", gfs.SendCopyArg{
	Handle:  handle,
	Address: to}, &reply)*/
	err = csm.CallChunkServerByAddr(from, "ChunkServer.RPCSendCopy", gfs.SendCopyArg{
		Handle:  handle,
		Address: to}, &reply)
	if err != nil {
		return err
	}
	chunk, err := csm.cm.OpenChunk(handle, gfs.OpenReadWrite)
	if err != nil {
		return err
	}
	defer chunk.Close()
	if lease, _ := chunk.GetLeaseHolder(); lease != nil {
		chunk.MarkInDanger()
	}

	csm.RLock()
	csInfo, ok := csm.servers[to]
	csm.RUnlock()
	if !ok {
		return gfs.Error{Code: gfs.UnknownError, Err: fmt.Sprintf("No such server %v", to)}
	}
	csInfo.Lock()
	if atomic.LoadInt32(&csInfo.dead) == 0 {
		csInfo.chunks[handle] = true
		chunk.RegisterReplica(to)
	}
	csInfo.Unlock()

	if chunk.CheckInDanger() && chunk.GetReplicas().Size() >= gfs.DefaultNumReplicas {
		chunk.UnmarkInDanger()
	}
	return nil
}

// ChooseServers returns servers to store new chunk.
// It is called when a new chunk is create
func (csm *chunkServerManager) ChooseServers(num int) (ret []gfs.ServerAddress, err error) {
	//TRICK map ergodic in golang is randomly
	csm.RLock()
	defer csm.RUnlock()
	numChoose := num
	for k, v := range csm.servers {
		if atomic.LoadInt32(&v.dead) == 1 {
			continue
		}
		if num <= 0 {
			break
		}
		ret = append(ret, k)
		num--
	}
	err = nil
	if num == numChoose {
		err = gfs.Error{Code: gfs.NoAvailableServer, Err: "No available chunkserver"}
	}
	return
}

// DetectDeadServers detects disconnected chunkservers according to last heartbeat time
func (csm *chunkServerManager) DetectDeadServers() {
	var dead []gfs.ServerAddress
	var failed []gfs.ServerAddress
	csm.RLock()
	ti := time.Now()
	for k, v := range csm.servers {
		v.RLock()
		if ti.After(v.lastHeartbeat.Add(gfs.ServerCleanupTime)) {
			failed = append(failed, k)
		}
		if ti.After(v.lastHeartbeat.Add(gfs.ServerTimeout)) && atomic.LoadInt32(&v.dead) == 0 {
			dead = append(dead, k)
		}
		v.RUnlock()
	}
	csm.RUnlock()
	if len(dead) != 0 {
		log.Debugf("Dead servers: %v", dead)
	}
	for _, v := range dead {
		csm.RLock()
		csInfo, ok := csm.servers[v]
		csm.RUnlock()
		if !ok {
			continue
		}
		var chunks []gfs.ChunkHandle

		if csInfo.deadlock.TryLock() {
			csInfo.Lock()
			for h := range csInfo.chunks {
				chunks = append(chunks, h)
			}
			for _, h := range chunks {
				delete(csInfo.chunks, h)
			}
			atomic.StoreInt32(&csInfo.dead, 1)
			atomic.StoreInt32(&csInfo.stability, csInfo.stability/2)
			csInfo.Unlock()
			csInfo.deadlock.Unlock()
		}
		for _, h := range chunks {
			chunk, e := csm.cm.OpenChunk(h, gfs.OpenReadWrite)
			if e != nil {
				continue
			}
			chunk.UnregisterReplica(v)
			chunk.Close()
		}
	}

	for _, v := range failed {
		csm.RemoveServer(v)
	}
	return
}

// RemoveServers removes metedata of a disconnected chunkserver.
// It returns the chunks that server holds
func (csm *chunkServerManager) RemoveServer(addr gfs.ServerAddress) (handles []gfs.ChunkHandle, err error) {
	csm.Lock()
	defer csm.Unlock()
	z, ok := csm.servers[addr]
	if !ok {
		err = fmt.Errorf("Unknown Server Address!")
		return
	}
	z.Lock()
	defer z.Unlock()
	for k, v := range z.chunks {
		if v {
			handles = append(handles, k)
		}
	}
	delete(csm.servers, addr)
	return
}

func (csm *chunkServerManager) GrantLeases(chunks []*gfsChunk, primary gfs.ServerAddress, length time.Duration) (leases []*lease, reterr []error, allerr error) {
	reterr = make([]error, len(chunks))
	handles := make([]gfs.ChunkHandle, len(chunks))
	for idx := 0; idx < len(chunks); idx++ {
		if chunks[idx] == nil {
			reterr[idx] = gfs.Error{Code: gfs.ArgumentError, Err: "Null Chunk"}
			continue
		}
		handles[idx] = chunks[idx].GetHandle()
	}

	log.Debugf("\033[32;1m[FuncCall|GrantLeases] handles: %+v, primary: %s\033[m", handles, primary)
	defer func() { log.Debugf("\033[32m[FuncRet |GrantLeases] reterr: %v, allerr: %v\033[m", reterr, allerr) }()

	defer func() {
		cnt := 0
		if allerr == nil {
			for idx := 0; idx < len(chunks); idx++ {
				if reterr[idx] == nil {
					cnt++
				}
			}
		}
		log.Infof("Grant %d Leases to %s, %d leases succeed (length=%v)", len(chunks), primary, cnt, length)
	}()

	leases = make([]*lease, len(handles))
	var glea []gfs.GrantLeaseArgLeasesType
	for idx := 0; idx < len(handles); idx++ {
		if reterr[idx] != nil {
			continue
		}
		if !chunks[idx].HasReplica(primary) {
			reterr[idx] = gfs.Error{Code: gfs.InvalidReplica, Err: fmt.Sprintf("Chunkserver '%s' has no valid replica for chunk %v", primary, handles[idx])}
			continue
		}
		if curLease, err := chunks[idx].GetLeaseHolder(); err == nil && curLease.primary != primary {
			reterr[idx] = gfs.Error{
				Code: gfs.LeaseNotExpired,
				Err: fmt.Sprintf("Cannot grant chunk %v lease to %s, current lease is not expired (primary %s)",
					handles[idx], primary, curLease.primary)}
			leases[idx] = curLease
			continue
		}
		ver, _ := chunks[idx].Info()
		expire := time.Now().Add(length)
		glea = append(glea, gfs.GrantLeaseArgLeasesType{
			Handle:     handles[idx],
			Expire:     expire,
			NewVersion: ver + 1})
	}

	var reply gfs.GrantLeaseReply
	//err := util.Call(primary, "ChunkServer.RPCGrantLease", gfs.GrantLeaseArg{Leases: glea}, &reply)
	err := csm.CallChunkServerByAddr(primary, "ChunkServer.RPCGrantLease", gfs.GrantLeaseArg{Leases: glea}, &reply)
	if err != nil {
		gfserr, ok := err.(gfs.Error)
		if ok {
			return nil, nil, gfserr
		}
		return nil, nil, gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to call RPC: %v", err)}
	}
	if len(reply.Errors) != len(glea) {
		//So Bad!!!
		return nil, nil, gfs.Error{Code: gfs.UnknownError, Err: "Broken RPC Reply"}
	}

	secondaries := make(map[gfs.ServerAddress]*struct {
		args    gfs.UpdateVersionArg
		indices []int
	})
	for idx, i := 0, 0; idx < len(handles); idx++ {
		if reterr[idx] != nil {
			continue
		}
		if reply.Errors[i] == nil {
			replicas := chunks[idx].GetReplicas()
			newver := chunks[idx].AddVersion()
			for _, v := range replicas.GetAll() {
				addr := v.(gfs.ServerAddress)
				if addr == primary {
					continue
				}
				tmp, ok := secondaries[addr]
				if !ok {
					tmp = new(struct {
						args    gfs.UpdateVersionArg
						indices []int
					})
					secondaries[addr] = tmp
				}
				tmp.args.Chunks = append(tmp.args.Chunks, gfs.UpdateVersionArgChunksType{
					Handle:     handles[idx],
					NewVersion: newver})
				tmp.indices = append(tmp.indices, idx)
			}
			leases[idx] = &lease{
				primary: primary,
				expire:  glea[i].Expire}
		} else {
			reterr[idx] = reply.Errors[i]
		}
	}

	for addr, tmp := range secondaries {
		var reply gfs.UpdateVersionReply
		//err := util.Call(addr, "ChunkServer.RPCUpdateVersion", tmp.args, &reply)
		err := csm.CallChunkServerByAddr(addr, "ChunkServer.RPCUpdateVersion", tmp.args, &reply)
		if err == nil && len(tmp.args.Chunks) == len(reply.Errors) {
			for i := 0; i < len(tmp.args.Chunks); i++ {
				if reply.Errors[i] != nil {
					chunks[tmp.indices[i]].UnregisterReplica(addr)
					csm.UnregisterReplica(addr, handles[tmp.indices[i]])
				} else {
					leases[tmp.indices[i]].secondaries = append(leases[tmp.indices[i]].secondaries, addr)
				}
			}
		} else {
			log.Debugf("GrantLeases: Call UpdateVersion to %s: %v", addr, err)
			for i := 0; i < len(tmp.args.Chunks); i++ {
				chunks[tmp.indices[i]].UnregisterReplica(addr)
				csm.UnregisterReplica(addr, handles[tmp.indices[i]])
			}
		}
	}

	for idx := 0; idx < len(handles); idx++ {
		if leases[idx] != nil && reterr[idx] == nil {
			chunks[idx].UpdateLease(leases[idx])
		}
	}

	return
}

func (csm *chunkServerManager) GrantLeasesByHandle(handles []gfs.ChunkHandle, primary gfs.ServerAddress, extend bool) (leases []*lease, reterr []error, allerr error) {
	length, allerr := csm.GetLeaseLength(primary)
	if allerr != nil {
		return
	}

	var chunks []*gfsChunk
	chunks, reterr = csm.cm.OpenChunks(handles, gfs.OpenReadWrite)

	defer func() {
		for _, v := range chunks {
			if v != nil {
				v.Close()
			}
		}
	}()

	if extend {
		for idx := 0; idx < len(handles); idx++ {
			if chunks[idx] != nil && chunks[idx].CheckInDanger() {
				chunks[idx].Close()
				chunks[idx] = nil
				reterr[idx] = gfs.Error{Code: gfs.ChunkInDanger, Err: fmt.Sprintf("Chunk %v is in danger and the lease will not extend", handles[idx])}
			}
		}
	}

	var tmperr []error
	leases, tmperr, allerr = csm.GrantLeases(chunks, primary, length)

	if allerr != nil {
		return leases, tmperr, allerr
	}

	for idx := 0; idx < len(handles); idx++ {
		if reterr[idx] != nil {
			continue
		}
		reterr[idx] = tmperr[idx]
	}
	return
}

func (csm *chunkServerManager) AddFailedChunk(handle gfs.ChunkHandle, addr gfs.ServerAddress) {
	csm.RLock()
	csInfo, ok := csm.servers[addr]
	csm.RUnlock()
	if !ok {
		return
	}
	csInfo.Lock()
	csInfo.failedChunks.Add(handle)
	csInfo.Unlock()
}

func (csm *chunkServerManager) UnregisterFailedChunks() {
	tmp := make(map[gfs.ServerAddress][]interface{})
	csm.RLock()
	for addr, csInfo := range csm.servers {
		csInfo.Lock()
		fc := csInfo.failedChunks.GetAllAndClear()
		for _, handle := range fc {
			delete(csInfo.chunks, handle.(gfs.ChunkHandle))
			csInfo.garbageChunks.Add(handle)
		}
		csInfo.Unlock()
		tmp[addr] = fc
	}
	csm.RUnlock()
	for addr, fc := range tmp {
		for _, handle := range fc {
			chunk, err := csm.cm.OpenChunk(handle.(gfs.ChunkHandle), gfs.OpenReadWrite)
			if err == nil {
				chunk.UnregisterReplica(addr)
				chunk.Close()
			}
		}
	}
}

func (csm *chunkServerManager) GetGarbageChunks(addr gfs.ServerAddress) (handles []gfs.ChunkHandle) {
	csm.RLock()
	csInfo, ok := csm.servers[addr]
	csm.RUnlock()
	if !ok {
		return
	}
	csInfo.RLock()
	tmp := csInfo.garbageChunks.GetAllAndClear()
	csInfo.RUnlock()
	for _, handle := range tmp {
		handles = append(handles, handle.(gfs.ChunkHandle))
	}
	return
}

func (csm *chunkServerManager) GetLeaseLength(primary gfs.ServerAddress) (time.Duration, error) {
	csm.RLock()
	csInfo, ok := csm.servers[primary]
	csm.RUnlock()
	if !ok {
		return 0, gfs.Error{Code: gfs.UnknownError, Err: fmt.Sprintf("No such server '%v'", primary)}
	}
	stability := int64(atomic.LoadInt32(&csInfo.stability))
	return time.Duration(stability*int64(gfs.LeaseExpire-gfs.LeaseExpireMin)/10000) + gfs.LeaseExpireMin, nil
}

func (csm *chunkServerManager) CallChunkServer(csInfo *chunkServerInfo, rpcname string, args interface{}, reply interface{}) error {
	csInfo.deadlock.RLock()
	defer csInfo.deadlock.RUnlock()
	if atomic.LoadInt32(&csInfo.dead) == 0 {
		err := util.Call(csInfo.addr, rpcname, args, reply)
		if err != nil {
			return err
		}
		return nil
	}
	return gfs.Error{Code: gfs.ServerDead, Err: fmt.Sprintf("Server %s has dead", csInfo.addr)}
}

func (csm *chunkServerManager) CallChunkServerByAddr(addr gfs.ServerAddress, rpcname string, args interface{}, reply interface{}) error {
	csm.RLock()
	csInfo, ok := csm.servers[addr]
	csm.RUnlock()
	if !ok {
		return gfs.Error{Code: gfs.UnknownError, Err: fmt.Sprintf("No such server %s", addr)}
	}
	return csm.CallChunkServer(csInfo, rpcname, args, reply)
}
