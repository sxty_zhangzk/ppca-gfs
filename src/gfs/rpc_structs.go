package gfs

import (
	"time"
)

//------ ChunkServer

type PushDataAndForwardArg struct {
	//Handle    ChunkHandle
	Data      []byte
	ForwardTo []ServerAddress
}
type PushDataAndForwardReply struct {
	DataID BufferHandle
}

type ForwardDataArg struct {
	DataID BufferHandle
	Data   []byte
}
type ForwardDataReply struct{}

type CreateChunkArg struct {
	Handle ChunkHandle
}
type CreateChunkReply struct{}

type WriteChunkArg struct {
	Handle      ChunkHandle
	DataID      BufferHandle
	Offset      Offset
	Secondaries []ServerAddress
}
type WriteChunkReply struct{}

type AppendChunkArg struct {
	Handle      ChunkHandle
	DataID      BufferHandle
	Secondaries []ServerAddress
}
type AppendChunkReply struct {
	Offset Offset
}

type ApplyMutationArg struct {
	Mtype    MutationType
	SerialNo ChunkVersion
	Handle   ChunkHandle
	DataID   BufferHandle
	Offset   Offset
	Length   int
}
type ApplyMutationReply struct{}

type PadChunkArg struct {
	Handle ChunkHandle
}
type PadChunkReply struct{}

type ReadChunkArg struct {
	Handle ChunkHandle
	Offset Offset
	Length int
}
type ReadChunkReply struct {
	Data   []byte
	Length int
}

type SendCopyArg struct {
	Handle  ChunkHandle
	Address ServerAddress
}
type SendCopyReply struct{}

type ApplyCopyArg struct {
	Handle   ChunkHandle
	Data     []byte
	Version  ChunkVersion
	SerialNo ChunkVersion
}
type ApplyCopyReply struct{}

type GrantLeaseArgLeasesType struct {
	Handle     ChunkHandle
	Expire     time.Time
	NewVersion ChunkVersion
}
type GrantLeaseArg struct {
	Leases []GrantLeaseArgLeasesType
}
type GrantLeaseReply struct {
	Errors []error
}

type UpdateVersionArgChunksType struct {
	Handle     ChunkHandle
	NewVersion ChunkVersion
}
type UpdateVersionArg struct {
	Chunks []UpdateVersionArgChunksType
}
type UpdateVersionReply struct {
	Errors []error
}

type ChunkReportType struct {
	Handle  ChunkHandle
	Version ChunkVersion
}
type GetChunkListArg struct {
}
type GetChunkListReply struct {
	Chunks []ChunkReportType
}

type GetBufferAddressArg struct{}
type GetBufferAddressReply struct {
	Address ServerAddress
}

//------ Master

type HeartbeatArg struct {
	Address         ServerAddress // chunkserver address
	LeaseExtensions []ChunkHandle // leases to be extended
	Chunks          []ChunkReportType
	FailedChunks    []ChunkHandle
}
type HeartbeatReply struct {
	Garbages []ChunkHandle
}

type GetPrimaryAndSecondariesArg struct {
	Handle ChunkHandle
}
type GetPrimaryAndSecondariesReply struct {
	Primary     ServerAddress
	Expire      time.Time
	Secondaries []ServerAddress
}

type ExtendLeaseArg struct {
	Handle  ChunkHandle
	Address ServerAddress
}
type ExtendLeaseReply struct {
	Expire time.Time
}

type GetReplicasArg struct {
	Handle ChunkHandle
}
type GetReplicasReply struct {
	Locations []ServerAddress
}

type GetFileInfoArg struct {
	Path Path
}
type GetFileInfoReply struct {
	IsDir  bool
	Length int64
	Chunks int64
}

type CreateFileArg struct {
	Path Path
}
type CreateFileReply struct {
}

type DeleteFileArg struct {
	Path Path
}
type DeleteFileReply struct {
}

type MkdirArg struct {
	Path Path
}
type MkdirReply struct {
}

type ListArg struct {
	Path Path
}
type ListReply struct {
	Files []PathInfo
}

type GetChunkHandleArg struct {
	Path  Path
	Index ChunkIndex
}
type GetChunkHandleReply struct {
	Handle ChunkHandle
}

type GetTimeArg struct{}
type GetTimeReply struct {
	Time time.Time
}
