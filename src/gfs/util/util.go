package util

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math"
	"math/rand"
	"net/rpc"
	"sync"
	"time"

	"gfs"

	log "github.com/Sirupsen/logrus"
)

// Call is RPC call helper
func Call(srv gfs.ServerAddress, rpcname string, args interface{}, reply interface{}) error {
	c, errx := rpc.Dial("tcp", string(srv))
	if errx != nil {
		return gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to dial: %v", errx)}
	}
	defer c.Close()

	done := c.Go(rpcname, args, reply, nil).Done

	select {
	case ret := <-done:
		if ret.Error == nil {
			return nil
		}
		buffer := bytes.NewBufferString(ret.Error.Error())
		dec := json.NewDecoder(buffer)
		var gfsErr gfs.Error
		if e := dec.Decode(&gfsErr); e != nil {
			return gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to call RPC %s on %s: %v", rpcname, srv, ret.Error)}
		}
		return gfsErr
	case <-time.After(gfs.RPCTimeout):
		return gfs.Error{Code: gfs.NetworkError, Err: "RPC Timeout"}
	}
}

func CallKeepAlive(client *rpc.Client, rpcname string, args interface{}, reply interface{}) error {
	done := client.Go(rpcname, args, reply, nil).Done
	select {
	case ret := <-done:
		if ret.Error == nil {
			return nil
		}
		buffer := bytes.NewBufferString(ret.Error.Error())
		dec := json.NewDecoder(buffer)
		var gfsErr gfs.Error
		if e := dec.Decode(&gfsErr); e != nil {
			return gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to call RPC %s on %v: %v", rpcname, client, ret.Error)}
		}
		return gfsErr
	case <-time.After(gfs.RPCTimeout):
		return gfs.Error{Code: gfs.NetworkError, Err: "RPC Timeout"}
	}
}

// CallAll applies the rpc call to all destinations.
func CallAll(dst []gfs.ServerAddress, rpcname string, args interface{}) error {
	ch := make(chan error)
	for _, d := range dst {
		go func(addr gfs.ServerAddress) {
			ch <- Call(addr, rpcname, args, nil)
		}(d)
	}
	errList := ""
	for _ = range dst {
		if err := <-ch; err != nil {
			errList += err.Error() + ";"
		}
	}

	if errList == "" {
		return nil
	}

	return fmt.Errorf(errList)
}

// Sample randomly chooses k elements from {0, 1, ..., n-1}.
// n should not be less than k.
func Sample(n, k int) ([]int, error) {
	if n < k {
		return nil, fmt.Errorf("population is not enough for sampling (n = %d, k = %d)", n, k)
	}
	return rand.Perm(n)[:k], nil
}

// SerializeError is used in RPC Callee, it serialize the gfs.Error type err to text format
func SerializeError(err *error) {
	if *err == nil {
		return
	}
	gfsErr, ok := (*err).(gfs.Error)
	if !ok {
		return
	}
	var buffer bytes.Buffer
	enc := json.NewEncoder(&buffer)
	e := enc.Encode(gfsErr)
	if e != nil {
		return
	}
	*err = fmt.Errorf("%s", buffer.String())
}

type RWMutexEx struct {
	rd, wr         *sync.Cond
	cntRd, cntWr   uint32
	waitRd, waitWr uint32
	waitRlock      uint32
	mtx            sync.Mutex
}

func NewRWMutexEx() *RWMutexEx {
	m := new(RWMutexEx)
	m.rd = sync.NewCond(&m.mtx)
	m.wr = sync.NewCond(&m.mtx)
	return m
}

func (m *RWMutexEx) InitMutex() {
	m.rd = sync.NewCond(&m.mtx)
	m.wr = sync.NewCond(&m.mtx)
}

func (m *RWMutexEx) Lock() {
	m.mtx.Lock()
	if m.cntWr != 0 || m.cntRd != 0 {
		m.waitWr++
		m.wr.Wait()
		m.waitWr--
	}
	m.cntWr++
	m.mtx.Unlock()
}
func (m *RWMutexEx) Unlock() {
	m.mtx.Lock()
	m.cntWr--
	if m.waitRd != 0 {
		m.rd.Broadcast()
	} else if m.waitWr != 0 {
		m.wr.Signal()
	}
	m.mtx.Unlock()
}

func (m *RWMutexEx) RLock() {
	m.mtx.Lock()
	if m.cntWr != 0 {
		m.waitRd++
		m.rd.Wait()
		m.waitRd--
	}
	m.cntRd++
	m.mtx.Unlock()
}
func (m *RWMutexEx) RUnlock() {
	m.mtx.Lock()
	m.cntRd--
	/*if m.waitRd != 0 {
		m.rd.Broadcast()
	} else*/
	if m.waitWr != 0 && m.cntRd == 0 {
		m.wr.Signal()
	}
	m.mtx.Unlock()
}

func (m *RWMutexEx) TryLock() bool {
	m.mtx.Lock()
	defer m.mtx.Unlock()
	if m.cntWr != 0 || m.cntRd != 0 {
		return false
	}
	m.cntWr++
	return true
}
func (m *RWMutexEx) TryRLock() bool {
	m.mtx.Lock()
	defer m.mtx.Unlock()
	if m.cntWr != 0 {
		return false
	}
	m.cntRd++
	return true
}

func SyncTime(master gfs.ServerAddress) (int64, bool) {
	c, err := rpc.Dial("tcp", string(master))
	if err != nil {
		return 0, false
	}
	//offset = client - master
	offset := make([]time.Duration, 12)
	var average, std float64
	for i := 0; i < len(offset); i++ {
		var reply gfs.GetTimeReply
		timeBefore := time.Now()
		err = CallKeepAlive(c, "Master.RPCGetTime", gfs.GetTimeArg{}, &reply)
		timeAfter := time.Now()
		if err != nil {
			c.Close()
			return 0, false
		}
		offset[i] = timeBefore.Add(timeAfter.Sub(timeBefore) / 2).Sub(reply.Time)
		average += float64(offset[i])
	}
	c.Close()
	average /= float64(len(offset))
	for i := 0; i < len(offset); i++ {
		std += (float64(offset[i]) - average) * (float64(offset[i]) - average)
	}
	std = math.Sqrt(std / float64(len(offset)-1))
	if average < 1e5 && std < 1e5 {
		log.Infof("Sync Time: Offset = %d ms, Std = %d ms", int(average/1e6), int(std/1e6))
		return 0, true
	}
	if std > 0.2*math.Abs(average) {
		log.Warningf("Sync Time: Offset = %d ms, Std = %d ms [abandoned]", int(average/1e6), int(std/1e6))
		return 0, false
	}
	log.Infof("Sync Time: Offset = %d ms, Std = %d ms", int(average/1e6), int(std/1e6))
	return int64(average), true
}
