package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gfs"
	"gfs/util"
	"io"
	"math/rand"
	"net"
	"sync"
	"sync/atomic"
	"time"

	log "github.com/Sirupsen/logrus"
)

type lease struct {
	sync.Mutex
	primary     gfs.ServerAddress
	expire      time.Time
	secondaries []gfs.ServerAddress
}

// Client struct is the GFS client-side driver
type Client struct {
	timeOffset int64

	master       gfs.ServerAddress
	leases       map[gfs.ChunkHandle]*lease
	lock         sync.RWMutex
	cacheBufAddr map[gfs.ServerAddress]gfs.ServerAddress
	lockCache    sync.RWMutex
}

func (c *Client) getlease(args gfs.GetPrimaryAndSecondariesArg, reply *gfs.GetPrimaryAndSecondariesReply) (err error) {
	c.lock.RLock()
	v, ok := c.leases[args.Handle]
	c.lock.RUnlock()

	if ok {
		v.Lock()
		f := v.expire.After(time.Now().Add(-time.Duration(atomic.LoadInt64(&c.timeOffset))))
		v.Unlock()
		if f {
			reply.Primary, reply.Expire, reply.Secondaries = v.primary, v.expire, v.secondaries
			return nil
		}
	}
	c.lock.Lock()
	defer c.lock.Unlock()
	err = util.Call(c.master, "Master.RPCGetPrimaryAndSecondaries", args, reply)
	if err == nil {
		c.leases[args.Handle] = &lease{primary: reply.Primary, expire: reply.Expire, secondaries: reply.Secondaries}
	}
	return
}

// NewClient returns a new gfs client.
func NewClient(master gfs.ServerAddress) *Client {
	//log.SetLevel(log.DebugLevel)

	ret := new(Client)
	ret.master = master
	ret.cacheBufAddr = make(map[gfs.ServerAddress]gfs.ServerAddress)
	ret.leases = make(map[gfs.ChunkHandle]*lease)
	go func() {
		for {
			offset, ok := util.SyncTime(master)
			if ok {
				atomic.StoreInt64(&ret.timeOffset, offset)
				return
			}
			time.Sleep(gfs.RetryInterval)
		}
	}()
	return ret
}

// Create creates a new file on the specific path on GFS.
func (c *Client) Create(path gfs.Path) error {
	return util.Call(c.master, "Master.RPCCreateFile", gfs.CreateFileArg{Path: path}, &gfs.CreateFileReply{})
}

// Mkdir creates a new directory on GFS.
func (c *Client) Mkdir(path gfs.Path) error {
	return util.Call(c.master, "Master.RPCMkdir", gfs.MkdirArg{Path: path}, &gfs.MkdirReply{})
}

// List lists everything in specific directory on GFS.
func (c *Client) List(path gfs.Path) ([]gfs.PathInfo, error) {
	var ret gfs.ListReply
	err := util.Call(c.master, "Master.RPCList", gfs.ListArg{Path: path}, &ret)
	return ret.Files, err
}

// Read reads the file at specific offset.
// It reads up to len(data) bytes form the File.
// It return the number of bytes, and an error if any.
func Choose(adds gfs.GetReplicasReply) (ret1 gfs.ServerAddress, ret2 gfs.GetReplicasReply) {
	r := rand.Intn(len(adds.Locations))
	ret1 = adds.Locations[r]
	ret2.Locations = append(adds.Locations[:r], adds.Locations[r+1:]...)
	return ret1, ret2
}
func (c *Client) Read(path gfs.Path, offset gfs.Offset, data []byte) (n int, err error) {
	var fileinfo gfs.GetFileInfoReply
	for {
		err = util.Call(c.master, "Master.RPCGetFileInfo", gfs.GetFileInfoArg{Path: path}, &fileinfo)
		if err == nil {
			break
		}
		log.Debug("Retry get fileinfo!\n")
		time.Sleep(gfs.RetryInterval)
	}
	chunks := fileinfo.Chunks
	/*l := int(fileinfo.Length - int64(offset))
	if l > len(data) {
		l = len(data)
	}
	if l < 0 {
		n, err = 0, fmt.Errorf("Invalid Parameter!")
		return
	}*/
	l := len(data)
	now := int(offset) / gfs.MaxChunkSize
	if l == 0 {
		return 0, fmt.Errorf("cao")
	}
	o := 0
	type temp struct {
		n   int
		err error
	}
	//ch := make(chan temp)
	total := 0
	errlist := ""
	isEOF := false
	for l > 0 {
		m := gfs.MaxChunkSize - (o+int(offset))%gfs.MaxChunkSize
		if m > l {
			m = l
		}
		/*go*/ f := func(nw int, offs gfs.Offset, dat []byte) int {
			var handle gfs.ChunkHandle
			for {
				handle, err = c.GetChunkHandle(path, gfs.ChunkIndex(nw))
				if err == nil {
					break
				}
				log.Debugf("Retry get handle!\n")
				time.Sleep(gfs.RetryInterval)
			}
			var res temp
			res.n, res.err = c.ReadChunk(handle, offs, dat)
			//ch <- res
			if res.err != nil {
				return 0
			}
			n += res.n
			//log.Debugf("res.n=%d len(dat)=%d", res.n, len(dat))
			if res.n < len(dat) {
				return 1
			}
			return 2
		}(now, gfs.Offset((o+int(offset))%gfs.MaxChunkSize), data[o:o+m])
		if f == 1 {
			isEOF = true
			break
		}
		if f == 0 {
			log.Debugf("Retry ReadChunk!\n")
			time.Sleep(gfs.RetryInterval)
			continue
		}
		l -= m
		o += m
		now++
		if int64(now) > chunks {
			break
		}
		total++
	}
	/*for i := 0; i < total; i++ {
		t := <-ch
		n += t.n
		if t.err != nil {
			errlist += t.err.Error() + ";"
		}
	}*/
	if errlist != "" {
		err = fmt.Errorf(errlist)
	}
	//log.Errorf("n=%d", n)
	if isEOF {
		err = io.EOF
	}
	return
}

// Write writes data to the file at specific offset.
func (c *Client) Write(path gfs.Path, offset gfs.Offset, data []byte) (err error) {
	now := int(offset) / gfs.MaxChunkSize
	l := len(data)
	o := 0
	//ch := make(chan error)
	total := 0
	errlist := ""
	for l > 0 {
		m := gfs.MaxChunkSize - (o+int(offset))%gfs.MaxChunkSize
		if m > l {
			m = l
		}
		func(nw int, offs gfs.Offset, dat []byte) (err error) {
			var handle gfs.ChunkHandle
			for {
				handle, err = c.GetChunkHandle(path, gfs.ChunkIndex(nw))
				if err == nil {
					break
				}
				log.Debugf("Retry get handle!\n")
				time.Sleep(gfs.RetryInterval)
			}
			//ch <- c.WriteChunk(handle, offs, dat)
			err = c.WriteChunk(handle, offs, dat)
			if err != nil {
				errlist += err.Error() + ";"
			}
			return
		}(now, gfs.Offset((o+int(offset))%gfs.MaxChunkSize), data[o:o+m])
		l -= m
		o += m
		now++
		total++
	}
	/*for i := 0; i < total; i++ {
		if e := <-ch; e != nil {
			errlist += e.Error() + ";"
		}
	}*/
	if errlist != "" {
		err = fmt.Errorf(errlist)
	}
	return
}

// Append appends data to the file. Offset of the beginning of appended data is returned.
func (c *Client) Append(path gfs.Path, data []byte) (offset gfs.Offset, err error) {
	offset, err = gfs.Offset(0), nil
	if len(data) > gfs.MaxAppendSize {
		err = fmt.Errorf("Append Overflow!")
		return
	}
	var reply gfs.GetFileInfoReply
	for {
		err = util.Call(c.master, "Master.RPCGetFileInfo", gfs.GetFileInfoArg{Path: path}, &reply)
		if err == nil {
			break
		}
		log.Debug("Retry get fileinfo!\n")
		time.Sleep(gfs.RetryInterval)
	}
	var cid gfs.ChunkIndex
	if reply.Chunks == 0 {
		cid = 0
	} else {
		cid = gfs.ChunkIndex(reply.Chunks - 1)
	}
	for {
		var handle gfs.ChunkHandle
		for {
			handle, err = c.GetChunkHandle(path, cid)
			if err == nil {
				break
			}
			log.Debugf("Retry get handle!\n")
			time.Sleep(gfs.RetryInterval)
		}
		var chunkOffset gfs.Offset
		chunkOffset, err = c.AppendChunk(handle, data)
		if err == nil {
			offset = gfs.Offset(int(cid)*gfs.MaxChunkSize) + chunkOffset
			return
		} else if err != nil && err.(gfs.Error).Code == gfs.AppendExceedChunkSize {
			cid++
			continue
		}
		log.Debugf("Append: FUCK: %v %v", err.(gfs.Error).Code, err)
		return
	}
}

// GetChunkHandle returns the chunk handle of (path, index).
// If the chunk doesn't exist, master will create one.
func (c *Client) GetChunkHandle(path gfs.Path, index gfs.ChunkIndex) (gfs.ChunkHandle, error) {
	var ret gfs.GetChunkHandleReply
	err := util.Call(c.master, "Master.RPCGetChunkHandle", gfs.GetChunkHandleArg{Path: path, Index: index}, &ret)
	return ret.Handle, err
}

// ReadChunk reads data from the chunk at specific offset.
// len(data)+offset  should be within chunk size.
func (c *Client) ReadChunk(handle gfs.ChunkHandle, offset gfs.Offset, data []byte) (n int, err error) {
	n, err = 0, nil
	if len(data)+int(offset) > gfs.MaxChunkSize {
		err = fmt.Errorf("Read overflow!")
		return
	}
	var replicas gfs.GetReplicasReply
	err = util.Call(c.master, "Master.RPCGetReplicas", gfs.GetReplicasArg{Handle: handle}, &replicas)
	if err != nil {
		return
	}
	if len(replicas.Locations) == 0 {
		err = fmt.Errorf("naive")
		return
	}
	chunkpath, _ := Choose(replicas)
	var reply gfs.ReadChunkReply
	err = util.Call(chunkpath, "ChunkServer.RPCReadChunk", gfs.ReadChunkArg{Handle: handle, Offset: offset, Length: len(data)}, &reply)
	if err != nil {
		return
	}
	//data = reply.Data
	copy(data, reply.Data)
	n = reply.Length
	return
}

// WriteChunk writes data to the chunk at specific offset.
// len(data)+offset should be within chunk size.
func (c *Client) WriteChunk(handle gfs.ChunkHandle, offset gfs.Offset, data []byte) (err error) {
	defer func() {
		if err != nil {
			log.Error("while WriteChunk", handle, err)
		}
	}()
	err = nil
	if len(data)+int(offset) > gfs.MaxChunkSize {
		err = fmt.Errorf("Write overflow!")
		return
	}
	// TODO: Get Primary First //done
	fault := 3
	var GPS gfs.GetPrimaryAndSecondariesReply
	var replicas gfs.GetReplicasReply
	var reply gfs.PushDataAndForwardReply
	var ret gfs.WriteChunkReply
	for {
		if fault&1 != 0 {
			GPS = gfs.GetPrimaryAndSecondariesReply{}
			//err = util.Call(c.master, "Master.RPCGetPrimaryAndSecondaries", gfs.GetPrimaryAndSecondariesArg{Handle: handle}, &GPS)
			for {
				err = c.getlease(gfs.GetPrimaryAndSecondariesArg{Handle: handle}, &GPS)
				if err == nil {
					break
				}
				time.Sleep(gfs.RetryInterval)
				continue
			}
			fault--
		}
		if fault != 0 {
			replicas.Locations = GPS.Secondaries
			replicas.Locations = append(replicas.Locations, GPS.Primary)
			if len(replicas.Locations) == 0 {
				err = fmt.Errorf("naive")
				return
			}
			chunkpath, replicas := Choose(replicas)

			//err = util.Call(chunkpath, "ChunkServer.RPCPushDataAndForward", gfs.PushDataAndForwardArg{Data: data, ForwardTo: replicas.Locations}, &reply)
			reply.DataID, err = c.pushData(chunkpath, data, replicas.Locations)
			if err != nil {
				c.lock.Lock()
				delete(c.leases, handle) //
				c.lock.Unlock()
				if err.(gfs.Error).Code == gfs.NetworkError || err.(gfs.Error).Code == gfs.DispatchError {
					time.Sleep(gfs.RetryInterval)
					fault = 3
					continue
				}
			}
			fault -= 2
		}

		err = util.Call(GPS.Primary, "ChunkServer.RPCWriteChunk", gfs.WriteChunkArg{
			Handle:      handle,
			DataID:      reply.DataID,
			Offset:      offset,
			Secondaries: GPS.Secondaries}, &ret)
		if err != nil {
			if err.(gfs.Error).Code == gfs.NetworkError || err.(gfs.Error).Code == gfs.DispatchError ||
				err.(gfs.Error).Code == gfs.FileSystemError {
				fault = 3
				time.Sleep(gfs.RetryInterval)
				continue
			}
			if err.(gfs.Error).Code == gfs.BufferNotFound {
				fault += 2
			} else {
				if err.(gfs.Error).Code == gfs.NotPrimary {
					c.lock.Lock()
					delete(c.leases, handle) //
					c.lock.Unlock()
					fault++
				} else {
					return
				}
			}
		}
		if fault == 0 {
			return
		}
	}
}

// AppendChunk appends data to a chunk.
// Chunk offset of the start of data will be returned if success.
// len(data) should be within max append size.
func (c *Client) AppendChunk(handle gfs.ChunkHandle, data []byte) (offset gfs.Offset, err error) {
	offset, err = gfs.Offset(0), nil
	if len(data) > gfs.MaxAppendSize {
		err = gfs.Error{Code: gfs.ArgumentError, Err: "Append Overflow!"}
		return
	}
	fault := 3
	var GPS gfs.GetPrimaryAndSecondariesReply
	var replicas gfs.GetReplicasReply
	var reply gfs.PushDataAndForwardReply
	var ret gfs.AppendChunkReply
	for {
		if fault&1 != 0 {
			GPS = gfs.GetPrimaryAndSecondariesReply{}
			//err = util.Call(c.master, "Master.RPCGetPrimaryAndSecondaries", gfs.GetPrimaryAndSecondariesArg{Handle: handle}, &GPS)
			for {
				err = c.getlease(gfs.GetPrimaryAndSecondariesArg{Handle: handle}, &GPS)
				if err == nil {
					break
				}
				time.Sleep(gfs.RetryInterval)
				continue
			}
			fault--
		}

		if fault != 0 {
			replicas.Locations = GPS.Secondaries
			replicas.Locations = append(replicas.Locations, GPS.Primary)
			//replicas.Locations = append(replicas.Locations, GPS.Secondaries...)
			log.Debug(replicas.Locations)
			if len(replicas.Locations) == 0 {
				err = fmt.Errorf("naive")
				return
			}
			chunkpath, replicas := Choose(replicas)
			//err = util.Call(chunkpath, "ChunkServer.RPCPushDataAndForward", gfs.PushDataAndForwardArg{Data: data, ForwardTo: replicas.Locations}, &reply)
			reply.DataID, err = c.pushData(chunkpath, data, replicas.Locations)
			if err != nil {
				c.lock.Lock()
				delete(c.leases, handle) //
				c.lock.Unlock()
				if err.(gfs.Error).Code == gfs.NetworkError || err.(gfs.Error).Code == gfs.DispatchError {
					time.Sleep(gfs.RetryInterval)
					fault = 3
					continue
				}
			}
			fault -= 2
		}
		err = util.Call(GPS.Primary, "ChunkServer.RPCAppendChunk", gfs.AppendChunkArg{
			Handle:      handle,
			DataID:      reply.DataID,
			Secondaries: GPS.Secondaries}, &ret)
		if err != nil {
			if err.(gfs.Error).Code == gfs.NetworkError || err.(gfs.Error).Code == gfs.DispatchError ||
				err.(gfs.Error).Code == gfs.FileSystemError {
				fault = 3
				time.Sleep(gfs.RetryInterval)
				continue
			}
			if err.(gfs.Error).Code == gfs.BufferNotFound {
				fault += 2
			} else if err.(gfs.Error).Code == gfs.NotPrimary {
				c.lock.Lock()
				delete(c.leases, handle) //
				c.lock.Unlock()
				fault++
			} else {
				return
			}
		}
		if fault == 0 {
			offset = ret.Offset
			return
		}
	}
}

func (c *Client) getBufferAddr(addr gfs.ServerAddress) (string, error) {
	c.lockCache.RLock()
	bufaddr, ok := c.cacheBufAddr[addr]
	c.lockCache.RUnlock()
	if !ok {
		var reply gfs.GetBufferAddressReply
		err := util.Call(addr, "ChunkServer.RPCGetBufferAddress", gfs.GetBufferAddressArg{}, &reply)
		if err != nil {
			return "", err
		}
		c.lockCache.Lock()
		c.cacheBufAddr[addr] = reply.Address
		c.lockCache.Unlock()
		return string(reply.Address), nil
	}
	return string(bufaddr), nil
}

func (c *Client) pushData(addr gfs.ServerAddress, data []byte, forwardTo []gfs.ServerAddress) (bufhandle gfs.BufferHandle, reterr error) {
	if len(data) <= gfs.RPCPushDataLimit {
		var reply gfs.PushDataAndForwardReply
		err := util.Call(addr, "ChunkServer.RPCPushDataAndForward", gfs.PushDataAndForwardArg{
			Data:      data,
			ForwardTo: forwardTo}, &reply)
		if err != nil {
			return 0, err
		}
		return reply.DataID, nil
	}

	defer func() {
		if reterr != nil {
			log.Debugf("Push Data: addr %s forwardTo %v err %v", addr, forwardTo, reterr)
			c.lockCache.Lock()
			delete(c.cacheBufAddr, addr)
			for _, v := range forwardTo {
				delete(c.cacheBufAddr, v)
			}
			c.lockCache.Unlock()
		}
	}()

	srvAddr, err := c.getBufferAddr(addr)
	if err != nil {
		return 0, err
	}
	fwAddr := make([]string, len(forwardTo))
	for i, v := range forwardTo {
		fwAddr[i], err = c.getBufferAddr(v)
		if err != nil {
			return 0, err
		}
	}

	raddr, err := net.ResolveTCPAddr("tcp", string(srvAddr))
	if err != nil {
		return 0, gfs.Error{Code: gfs.NetworkError, Err: err.Error()}
	}
	conn, err := net.DialTCP("tcp", nil, raddr)
	if err != nil {
		return 0, gfs.Error{Code: gfs.NetworkError, Err: err.Error()}
	}
	defer conn.Close()
	buffer := new(bytes.Buffer)
	enc := json.NewEncoder(buffer)
	err = enc.Encode(gfs.BufferRequest{
		Req:       gfs.ReqPushData,
		Handle:    "0",
		Length:    uint32(len(data)),
		ForwardTo: fwAddr})
	if err != nil {
		return 0, gfs.Error{Code: gfs.UnknownError, Err: fmt.Sprintf("An error occurred when encoding request: %v", err)}
	}

	n, err := conn.Write(buffer.Bytes())
	if err != nil {
		return 0, gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to send request: %v", err)}
	} else if n != buffer.Len() {
		return 0, gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to send request: send %d bytes, expected %d bytes", n, buffer.Len())}
	}

	for off := 0; off < len(data); off += gfs.TCPRWSize {
		length := len(data) - off
		if length > gfs.TCPRWSize {
			length = gfs.TCPRWSize
		}
		n, err := conn.Write(data[off : off+length])
		if err != nil {
			return 0, gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to send data: %v", err)}
		} else if n != length {
			return 0, gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to send data: send %d bytes, expected %d bytes", n, length)}
		}
	}
	log.Debugf("Write done. Wait for response")

	tmp := make([]byte, 30)
	n, err = conn.Read(tmp)
	if err == io.EOF && n == 0 {
		return 0, gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to send data: empty response")}
	} else if err != nil && err != io.EOF {
		return 0, gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to receive response: %v", err)}
	}
	if tmp[n-1] != '\n' {
		return 0, gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to receive response: invalid response '%s'", string(tmp))}
	}
	var handle gfs.BufferHandle
	_, err = fmt.Sscanf(string(tmp[:n]), "%d", &handle)
	if err != nil {
		return 0, gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to parse response: %v", err)}
	}
	if fmt.Sprint(handle) != string(tmp[:n-1]) {
		return 0, gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to parse response: '%s' != %d", string(tmp), handle)}
	}

	return handle, nil
}
