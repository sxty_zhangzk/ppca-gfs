package chunkserver

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gfs"
	"io"
	"math/rand"
	"net"
	"sync"
	"sync/atomic"
	"time"

	log "github.com/Sirupsen/logrus"
)

type downloadItem struct {
	remainTime int64
	data       []byte
}

type downloadBuffer struct {
	sync.RWMutex
	buffer map[gfs.BufferHandle]*downloadItem
	expire time.Duration
	tick   time.Duration

	serveConn *net.TCPListener
	dead      bool
	//id        string
}

// newDownloadBuffer returns a downloadBuffer. Default expire time is expire.
// The downloadBuffer will cleanup expired items every tick.
func newDownloadBuffer(expire, tick time.Duration /*, id string*/) *downloadBuffer {
	buf := &downloadBuffer{
		buffer: make(map[gfs.BufferHandle]*downloadItem),
		expire: expire,
		tick:   tick,
		//id:     id,
	}

	// cleanup
	go func() {
		lastTick := time.Now()
		ticker := time.Tick(tick)
		for {
			<-ticker
			now := time.Now()
			buf.RLock()
			for id, item := range buf.buffer {
				if atomic.LoadInt64(&item.remainTime) < 0 {
					buf.RUnlock()
					buf.Delete(id)
					buf.RLock()
				} else {
					atomic.AddInt64(&item.remainTime, int64(lastTick.Sub(now)))
				}
			}
			buf.RUnlock()
			lastTick = now
		}
	}()

	return buf
}

// allocate a new DataID for given handle
func (buf *downloadBuffer) New() gfs.BufferHandle {
	now := time.Now()
	handle := gfs.BufferHandle((uint64(now.Nanosecond()/1000+now.Second()*1000000+now.Minute()*60*1000000) << 32) | uint64(rand.Uint32()))
	return handle
}

func (buf *downloadBuffer) Set(handle gfs.BufferHandle, data []byte) {
	buf.Lock()
	defer buf.Unlock()
	buf.buffer[handle] = &downloadItem{
		data:       data,
		remainTime: int64(gfs.DownloadBufferExpire)}
}

func (buf *downloadBuffer) Get(handle gfs.BufferHandle) ([]byte, bool) {
	buf.Lock()
	defer buf.Unlock()
	item, ok := buf.buffer[handle]
	if !ok {
		return nil, ok
	}
	atomic.StoreInt64(&item.remainTime, int64(gfs.DownloadBufferExpire))
	return item.data, ok
}

func (buf *downloadBuffer) Delete(handle gfs.BufferHandle) {
	buf.Lock()
	defer buf.Unlock()
	//log.Debugf("Server %s delete buffer %d: %+v", buf.id, handle, buf.buffer[handle])
	delete(buf.buffer, handle)
}

func (buf *downloadBuffer) Shutdown() {
	if !buf.dead {
		buf.dead = true
		buf.serveConn.Close()
	}
}

func (buf *downloadBuffer) Serve(addr gfs.ServerAddress) (listenAddr gfs.ServerAddress, reterr error) {
	laddr, err := net.ResolveTCPAddr("tcp", string(addr))
	if err != nil {
		return "", gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to resolve address '%s': %v", addr, err)}
	}
	buf.serveConn, err = net.ListenTCP("tcp", laddr)
	if err != nil {
		return "", gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to listen at '%s': %v", addr, err)}
	}
	go func() {
		for {
			conn, err := buf.serveConn.AcceptTCP()
			if err != nil {
				if buf.dead {
					return
				}
				log.Errorf("An error occurred when accept TCP: %v", err)
				continue
			}
			conn.SetLinger(-1)
			go func() {
				remoteAddr := conn.RemoteAddr()
				err := buf.ServeConn(conn)
				if err != nil {
					log.Errorf("An error occurred when serve conn with '%s': %v", remoteAddr, err)
				} else {
					//time.Sleep(3 * time.Second)
				}
				conn.Close()
			}()
		}
	}()
	return gfs.ServerAddress(buf.serveConn.Addr().String()), nil
}

func (buf *downloadBuffer) ServeConn(conn *net.TCPConn) error {
	waitingforReq := true
	eof := false
	var request gfs.BufferRequest

	buffer := make([]byte, gfs.TCPRWSize)
	bufferIndex := 0
	reqBuffer := new(bytes.Buffer)
	var bufItem *downloadItem
	var handle gfs.BufferHandle

	var chanForwardBuffer chan *[]byte
	var chanForwardBufferEmpty chan *[]byte //experiment, hopefully it can reduce memory use during forwarding

	var chanForwardDone chan bool
	var forwardFailed uint32
	var forwardError error
	forward := false
	forwardWaitRet := true

	/*
		Protocol:
			Read data:
				* client send a request (gfs.BufferRequest, encoded by json, ended with '\n') with Req == gfs.ReqReadData and Handle
				* server simply send the data to client
			Push data:
				* client send a request (gfs.BufferRequest, encoded by json, ended with '\n') with Req == gfs.ReqPushData
					Length should be the length of the data
					Handle is optional, if it is set to 0, the server will generate a random handle
					ForwardTo is also optional, the server will forward the data in order of the array
				* client send the data to server
				* server send the handle of the buffer (ended with '\n') if the operation is completed successfully
			NOTE: If there's any error during the operation, the server will close the connection immediately
	*/

	for !eof {
		bufferIndex = 0
		readSize, err := conn.Read(buffer)
		if err == io.EOF && readSize == 0 {
			log.Debugf("Break EOF")
			break
		} else if err != nil && err != io.EOF {
			return gfs.Error{Code: gfs.NetworkError, Err: err.Error()}
		}
		if waitingforReq {
			requestGet := false
			for ; bufferIndex < len(buffer); bufferIndex++ {
				if buffer[bufferIndex] == '\n' {
					bufferIndex++
					requestGet = true
					break
				}
				reqBuffer.WriteByte(buffer[bufferIndex])
			}
			if requestGet {
				//log.Debugf("Get Request: %s", reqBuffer.String())
				dec := json.NewDecoder(reqBuffer)
				err = dec.Decode(&request)
				if err != nil {
					return gfs.Error{Code: gfs.UnknownError, Err: fmt.Sprintf("Failed to decode request: %v", err)}
				}
				waitingforReq = false

				_, err := fmt.Sscanf(request.Handle, "%d", &handle)
				if err != nil {
					return gfs.Error{Code: gfs.ArgumentError, Err: fmt.Sprintf("Failed to parse handle '%s': %v", request.Handle, err)}
				}
				if request.Req == gfs.ReqReadData {
					var ok bool
					buf.Lock()
					bufItem, ok = buf.buffer[handle]
					delete(buf.buffer, handle)
					buf.Unlock()
					if !ok {
						return gfs.Error{Code: gfs.BufferNotFound, Err: fmt.Sprintf("Buffer %v not found", handle)}
					}
					for off := 0; off < len(bufItem.data); off += gfs.TCPRWSize {
						length := len(bufItem.data) - off
						if length > gfs.TCPRWSize {
							length = gfs.TCPRWSize
						}
						n, err := conn.Write(bufItem.data[off : off+length])
						if n != length {
							return gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Send %d bytes, expected %d", n, length)}
						} else if err != nil {
							return gfs.Error{Code: gfs.NetworkError, Err: err.Error()}
						}
					}
					return nil
				} else if request.Req == gfs.ReqPushData {
					bufItem = new(downloadItem)
					if handle == 0 {
						handle = buf.New()
					}
					if len(request.ForwardTo) != 0 {
						forwardAddr, err := net.ResolveTCPAddr("tcp", request.ForwardTo[0])
						if err != nil {
							return gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to resolve forward address '%s': %v", request.ForwardTo[0], err)}
						}
						forwardConn, err := net.DialTCP("tcp", nil, forwardAddr)
						if err != nil {
							return gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to dial forward address '%s': %v", request.ForwardTo[0], err)}
						}

						sendReqBuffer := new(bytes.Buffer)
						enc := json.NewEncoder(sendReqBuffer)
						if len(request.ForwardTo) > 1 {
							err = enc.Encode(gfs.BufferRequest{
								Req:       gfs.ReqPushData,
								Handle:    fmt.Sprint(handle),
								Length:    request.Length,
								ForwardTo: request.ForwardTo[1:]})
						} else {
							err = enc.Encode(gfs.BufferRequest{
								Req:    gfs.ReqPushData,
								Handle: fmt.Sprint(handle),
								Length: request.Length})
						}
						if err != nil {
							forwardConn.Close()
							return gfs.Error{Code: gfs.UnknownError, Err: fmt.Sprintf("Failed to forward to '%s', json encode error: %v", request.ForwardTo[0], err)}
						}

						n, err := forwardConn.Write(sendReqBuffer.Bytes())
						if err != nil {
							forwardConn.Close()
							return gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to send forward request to '%s': %v", request.ForwardTo[0], err)}
						} else if n != sendReqBuffer.Len() {
							forwardConn.Close()
							return gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to send forward request to '%s', write %d bytes, expected %d bytes", request.ForwardTo[0], n, sendReqBuffer.Len())}
						}

						forward = true
						chanForwardDone = make(chan bool, 1)
						chanForwardBuffer = make(chan *[]byte, 3)
						chanForwardBufferEmpty = make(chan *[]byte, 3)
						for i := 0; i < 3; i++ {
							chanForwardBufferEmpty <- &[]byte{}
						}

						go func() {
							defer func() {
								forwardConn.Close()
								chanForwardDone <- true
							}()
							for {
								data, ok := <-chanForwardBuffer
								if !ok {
									break
								}
								//log.Debugf("FORWARD to %s: %v", request.ForwardTo[0], *data)
								n, err := forwardConn.Write(*data)
								if err != nil {
									forwardError = gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to forward to '%s': %v", request.ForwardTo[0], err)}
									atomic.StoreUint32(&forwardFailed, 1)
									return
								} else if n != len(*data) {
									forwardError = gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to forward to '%s': write %d bytes, expected %d bytes", request.ForwardTo[0], n, len(*data))}
									atomic.StoreUint32(&forwardFailed, 1)
									return
								}
								*data = []byte{}
								chanForwardBufferEmpty <- data
							}
							if forwardWaitRet {
								tmp := make([]byte, 30)
								n, err := forwardConn.Read(tmp)
								if err == io.EOF && n == 0 {
									forwardError = gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to forward to '%s': no response", request.ForwardTo[0])}
									return
								} else if err != nil && err != io.EOF {
									forwardError = gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to forward to '%s' when receiving response: %v (n=%d)", request.ForwardTo[0], err, n)}
									return
								}
								var retHandle gfs.BufferHandle
								_, err = fmt.Sscanf(string(tmp), "%d", &retHandle)
								if err != nil {
									forwardError = gfs.Error{Code: gfs.UnknownError, Err: fmt.Sprintf("Failed to forward to '%s': invalid response (%v)", request.ForwardTo[0], err)}
									return
								}
								if retHandle != handle {
									forwardError = gfs.Error{Code: gfs.UnknownError, Err: fmt.Sprintf("Failed to forward to '%s': invalid response (got '%s' expected %v)", request.ForwardTo[0], string(tmp), handle)}
									return
								}
							}
						}()
					}
				} else {
					return gfs.Error{Code: gfs.ArgumentError, Err: fmt.Sprintf("Invalid request id %d", request.Req)}
				}
			}
		}
		if bufferIndex < readSize { //request.Req == gfs.ReqPushData
			maxOffset := readSize
			if len(bufItem.data)+readSize-bufferIndex >= int(request.Length) {
				maxOffset = int(request.Length) - len(bufItem.data) + bufferIndex
				eof = true
			}
			bufItem.data = append(bufItem.data, buffer[bufferIndex:maxOffset]...)
			if forward {
				if atomic.LoadUint32(&forwardFailed) == 1 {
					return forwardError
				}
				fdBuf := <-chanForwardBufferEmpty
				*fdBuf = make([]byte, maxOffset-bufferIndex)
				copy(*fdBuf, buffer[bufferIndex:maxOffset])
				chanForwardBuffer <- fdBuf
			}
		}
	}
	//log.Debugf("Read Done! request=%+v", request)
	if len(bufItem.data) < int(request.Length) {
		if forward {
			forwardWaitRet = false
			close(chanForwardBuffer)
		}
		return gfs.Error{Code: gfs.ArgumentError, Err: fmt.Sprintf("Not enough data: got %d bytes expected %d bytes", len(bufItem.data), request.Length)}
	}
	if forward {
		close(chanForwardBuffer)
		<-chanForwardDone
		if forwardError != nil {
			return forwardError
		}
	}
	bufItem.remainTime = int64(gfs.DownloadBufferExpire)
	buf.Lock()
	//log.Debugf("Server %s get Buffer %d: %+v", buf.id, handle, bufItem)
	buf.buffer[handle] = bufItem
	buf.Unlock()
	conn.Write([]byte(fmt.Sprintf("%v\n", handle)))
	//log.Debugf("Serve Done! request=%+v", request)
	return nil
}
