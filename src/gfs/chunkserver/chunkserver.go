package chunkserver

import (
	"bytes"
	"encoding/binary"
	"encoding/gob"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/rpc"
	"os"
	"regexp"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	log "github.com/Sirupsen/logrus"

	"gfs"
	"gfs/util"
)

// ChunkServer struct
type ChunkServer struct {
	timeOffset int64

	address    gfs.ServerAddress // chunkserver address
	master     gfs.ServerAddress // master address
	dlAddr     gfs.ServerAddress
	serverRoot string // path to data storage
	l          net.Listener
	shutdown   chan struct{}
	dead       bool // set to true if server is shutdown

	dl                     *downloadBuffer                // expiring download buffer
	pendingLeaseExtensions *util.ArraySet                 // pending lease extension
	chunk                  map[gfs.ChunkHandle]*chunkInfo // chunk information
	chunkMutex             sync.RWMutex
	failedChunks           *util.ArraySet

	resyncTime chan bool
	masterDead bool
}

/*
type RWMutexDebug struct {
	rwlock    sync.RWMutex
	callstack []byte
	lock      sync.Mutex
}

func (mtx *RWMutexDebug) LockDebug() {
	mtx.lock.Lock()
	log.Debugf(">>> Locking... Last Locker: %v <<<", string(mtx.callstack))
	mtx.lock.Unlock()
	mtx.rwlock.Lock()
	mtx.lock.Lock()
	mtx.callstack = make([]byte, 250)
	runtime.Stack(mtx.callstack, false)
	mtx.lock.Unlock()
	log.Debugf(">>> Successfully Locked. <<<")
}
func (mtx *RWMutexDebug) RLockDebug() {
	mtx.lock.Lock()
	log.Debugf(">>> RLocking... Last Locker: %v <<<", string(mtx.callstack))
	mtx.lock.Unlock()
	mtx.rwlock.RLock()
	mtx.lock.Lock()
	mtx.callstack = make([]byte, 250)
	runtime.Stack(mtx.callstack, false)
	mtx.lock.Unlock()
	log.Debugf(">>> Successfully RLocked. <<<")
}
func (mtx *RWMutexDebug) Lock() {
	mtx.rwlock.Lock()
	mtx.lock.Lock()
	mtx.callstack = make([]byte, 250)
	runtime.Stack(mtx.callstack, false)
	mtx.lock.Unlock()
}

func (mtx *RWMutexDebug) Unlock() {
	mtx.rwlock.Unlock()
}

func (mtx *RWMutexDebug) RLock() {
	mtx.rwlock.RLock()
	mtx.lock.Lock()
	mtx.callstack = make([]byte, 250)
	runtime.Stack(mtx.callstack, false)
	mtx.lock.Unlock()
}

func (mtx *RWMutexDebug) RUnlock() {
	mtx.rwlock.RUnlock()
}*/

type chunkInfo struct {
	sync.RWMutex
	//RWMutexDebug
	cs           *ChunkServer
	handle       gfs.ChunkHandle
	length       gfs.Offset
	version      gfs.ChunkVersion // version number of the chunk in disk
	currentSN    gfs.ChunkVersion // current mutation serial number
	serialUpdate *sync.Cond       // call Broadcast() when serialNo update
	timer        bool             // true if there is a timer that inc the currentSN
	leaseExpire  time.Time

	lastReport bool
	//TODO: Add Error Count of FileSystemError
}

func (cinfo *chunkInfo) isPrimary() bool {
	return cinfo.leaseExpire.After(time.Now().Add(-time.Duration(atomic.LoadInt64(&cinfo.cs.timeOffset))))
}

// NewAndServe starts a chunkserver and return the pointer to it.
func NewAndServe(addr, masterAddr gfs.ServerAddress, serverRoot string) *ChunkServer {
	cs := &ChunkServer{
		address:    addr,
		shutdown:   make(chan struct{}),
		master:     masterAddr,
		serverRoot: serverRoot,
		dl:         newDownloadBuffer(gfs.DownloadBufferExpire, gfs.DownloadBufferTick /*, "CS"+string(addr[len(addr)-1:])*/),
		pendingLeaseExtensions: new(util.ArraySet),
		failedChunks:           new(util.ArraySet),
		chunk:                  make(map[gfs.ChunkHandle]*chunkInfo),
		resyncTime:             make(chan bool, 1),
	}
	gob.Register(gfs.Error{})
	//log.SetLevel(log.DebugLevel)

	if ch := cs.serverRoot[len(cs.serverRoot)-1]; ch != '/' && ch != '\\' {
		cs.serverRoot += "/"
	}

	err := cs.loadChunks()

	if err != nil {
		log.Fatal("Failed to load chunks: ", err)
		log.Exit(1)
	}
	log.Infof("Loaded %d chunks", len(cs.chunk))

	rpcs := rpc.NewServer()
	rpcs.Register(cs)
	l, e := net.Listen("tcp", string(cs.address))
	if e != nil {
		log.Fatal("listen error:", e)
		log.Exit(1)
	}
	cs.l = l

	dlListen := strings.Split(string(addr), ":")[0] + ":0"
	dlAddr, err := cs.dl.Serve(gfs.ServerAddress(dlListen))
	if err != nil {
		log.Fatal("dl listen error: ", err)
		log.Exit(1)
	}
	log.Infof("Buffer Listen at %s", dlAddr)
	cs.dlAddr = dlAddr

	// RPC Handler
	go func() {
		for {
			select {
			case <-cs.shutdown:
				return
			default:
			}
			conn, err := cs.l.Accept()
			if err == nil {
				go rpcs.ServeConn(conn)
			} else {
				//TODO: Handle Error
				// if chunk server is dead, ignores connection error
				if !cs.dead {
					log.Fatal(err)
				}
			}
		}
	}()

	cs.resyncTime <- true
	go func() {
		for {
			select {
			case <-cs.shutdown:
				return
			case <-cs.resyncTime:
				offset, ok := util.SyncTime(cs.master)
				if !ok {
					cs.resyncTime <- true
					time.Sleep(gfs.RetryInterval)
					continue
				}
				atomic.StoreInt64(&cs.timeOffset, offset)
			}
		}
	}()

	cs.Heartbeat()
	// Heartbeat
	go func() {
		//TODO: Use Long Connection
		for {
			select {
			case <-cs.shutdown:
				return
			default:
			}
			cs.Heartbeat()
			time.Sleep(gfs.HeartbeatInterval)
		}
	}()

	log.Infof("ChunkServer is now running. addr = %v, root path = %v, master addr = %v", addr, serverRoot, masterAddr)

	return cs
}

func (cs *ChunkServer) Heartbeat() {
	pe := cs.pendingLeaseExtensions.GetAllAndClear()
	le := make([]gfs.ChunkHandle, len(pe))
	for i, v := range pe {
		le[i] = v.(gfs.ChunkHandle)
	}
	fc := cs.failedChunks.GetAllAndClear()
	fcHandle := make([]gfs.ChunkHandle, len(fc))
	for i, v := range fc {
		fcHandle[i] = v.(gfs.ChunkHandle)
	}
	args := &gfs.HeartbeatArg{
		Address:         cs.address,
		LeaseExtensions: le,
		FailedChunks:    fcHandle}

	var chunks []*chunkInfo
	cs.chunkMutex.RLock()
	cnt := 0
	for handle, cinfo := range cs.chunk {
		if cnt >= gfs.HeartbeatChunkLimit {
			break
		}
		cnt++
		cinfo.RLock()
		cinfo.lastReport = true
		args.Chunks = append(args.Chunks, gfs.ChunkReportType{
			Handle:  handle,
			Version: cinfo.version})
		chunks = append(chunks, cinfo)
	}
	cs.chunkMutex.RUnlock()

	var reply gfs.HeartbeatReply
	if err := util.Call(cs.master, "Master.RPCHeartbeat", args, &reply); err != nil {
		log.Warningf("Heartbeat error: %v", err)
		for _, cinfo := range chunks {
			cinfo.lastReport = false
			cinfo.RUnlock()
		}
		cs.masterDead = true
		return
	}

	for _, cinfo := range chunks {
		cinfo.lastReport = false
		cinfo.RUnlock()
	}

	if cs.masterDead {
		cs.masterDead = false
		go func() { cs.resyncTime <- true }()
	}

	cs.chunkMutex.Lock()
	for _, handle := range reply.Garbages {
		cinfo, ok := cs.chunk[handle]
		if !ok {
			continue
		}
		cinfo.Lock()
		os.Remove(cs.serverRoot + getChunkFileName(handle))
		os.Remove(cs.serverRoot + getChunkInfoFileName(handle))
		delete(cs.chunk, handle)
		cinfo.Unlock()
	}
	//log.Debugf("[CS%s|Heartbeat] chunks: %+v (cleaned %d chunks)", cs.address[len(cs.address)-1:], cs.chunk, len(reply.Garbages))
	cs.chunkMutex.Unlock()
}

// Shutdown shuts the chunkserver down
func (cs *ChunkServer) Shutdown() {
	cs.dl.Shutdown()
	if !cs.dead {
		log.Warningf("ChunkServer %v shuts down", cs.address)
		cs.dead = true
		close(cs.shutdown)
		cs.l.Close()
	}
}

// RPCPushDataAndForward is called by client.
// It saves client pushed data to memory buffer and forward to all other replicas.
// Returns a DataID which represents the index in the memory buffer.
func (cs *ChunkServer) RPCPushDataAndForward(args gfs.PushDataAndForwardArg, reply *gfs.PushDataAndForwardReply) (rpcerr error) {
	if len(args.Data) > 20 {
		log.Debugf("\033[34;1m[CS%s|RPCCall|PushDataAndForward] Handle: %v, ForwardTo: %s, Data: [%v ... %v](len=%d)\033[m",
			cs.address[len(cs.address)-1:], args.ForwardTo, args.Data[0], args.Data[len(args.Data)-1], len(args.Data))
	} else {
		log.Debugf("\033[34;1m[CS%s|RPCCall|PushDataAndForward] Handle: %v, ForwardTo: %s, Data: %v(len=%d)\033[m",
			cs.address[len(cs.address)-1:], args.ForwardTo, args.Data, len(args.Data))
	}
	defer func() {
		log.Debugf("\033[34m[CS%s|RPCRet |PushDataAndForward] Reply: %+v, Rpcerr: %v\033[m", cs.address[len(cs.address)-1:], *reply, rpcerr)
	}()

	defer util.SerializeError(&rpcerr)

	dataID := cs.dl.New()
	cs.dl.Set(dataID, args.Data)

	err := util.CallAll(args.ForwardTo, "ChunkServer.RPCForwardData", gfs.ForwardDataArg{
		DataID: dataID,
		Data:   args.Data})

	if err != nil {
		return gfs.Error{Code: gfs.DispatchError, Err: fmt.Sprintf("Failed to forward data: %v", err)}
	}
	reply.DataID = dataID
	return nil
}

// RPCForwardData is called by another replica who sends data to the current memory buffer.
// TODO: This should be replaced by a chain forwarding.
func (cs *ChunkServer) RPCForwardData(args gfs.ForwardDataArg, reply *gfs.ForwardDataReply) (rpcerr error) {
	log.Debugf("\033[34;1m[CS%s|RPCCall|ForwardData] DataID: %+v\033[m", cs.address[len(cs.address)-1:], args.DataID)
	defer func() {
		log.Debugf("\033[34m[CS%s|RPCRet |ForwardData] Reply: %+v, Rpcerr: %v\033[m", cs.address[len(cs.address)-1:], *reply, rpcerr)
	}()

	defer util.SerializeError(&rpcerr)
	cs.dl.Set(args.DataID, args.Data)
	return nil
}

// RPCCreateChunk is called by master to create a new chunk given the chunk handle.
func (cs *ChunkServer) RPCCreateChunk(args gfs.CreateChunkArg, reply *gfs.CreateChunkReply) (rpcerr error) {
	defer util.SerializeError(&rpcerr)
	_, err := cs.newChunk(args.Handle)
	return err
}

// RPCReadChunk is called by client, read chunk data and return
func (cs *ChunkServer) RPCReadChunk(args gfs.ReadChunkArg, reply *gfs.ReadChunkReply) (rpcerr error) {
	log.Debugf("\033[33;1m[CS%s|RPCCall|ReadChunk] Args: %+v\033[m", cs.address[len(cs.address)-1:], args)
	defer func() {
		log.Debugf("\033[33m[CS%s|RPCRet |ReadChunk] Rpcerr: %v\033[m", cs.address[len(cs.address)-1:], rpcerr)
	}()

	defer util.SerializeError(&rpcerr)
	defer cs.reportFileSystemErr(args.Handle, &rpcerr)

	cinfo, err := cs.getChunk(args.Handle)
	if err != nil {
		return err
	}
	cinfo.RLock()
	defer cinfo.RUnlock()

	fchunk, err := os.Open(cs.serverRoot + getChunkFileName(args.Handle))
	if err != nil {
		return gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to open chunk file %v: %v", args.Handle, err)}
	}
	defer fchunk.Close()

	buffer := make([]byte, args.Length)
	n, err := fchunk.ReadAt(buffer, int64(args.Offset))
	if err == io.EOF {
		reply.Data = buffer[:n]
		reply.Length = n
		//return gfs.Error{Code: gfs.ReadEOF, Err: "Read EOF"}
		return nil
	}
	if err != nil {
		reply.Length = 0
		return gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to read chunk file %v: %v", args.Handle, err)}
	}
	reply.Data = buffer
	reply.Length = n
	return nil
}

// RPCWriteChunk is called by client
// applies chunk write to itself (primary) and asks secondaries to do the same.
func (cs *ChunkServer) RPCWriteChunk(args gfs.WriteChunkArg, reply *gfs.WriteChunkReply) (rpcerr error) {
	buf, _ := cs.getBuffer(args.DataID)
	if len(buf) > 20 {
		log.Debugf("\033[33;1m[CS%s|RPCCall|WriteChunk] DataID: %v-%d, Offset: %3d Sec: %v \033[34;1m Data: [%v ... %v] (len=%d)\033[m",
			cs.address[len(cs.address)-1:], args.Handle, args.DataID, args.Offset, args.Secondaries,
			buf[0], buf[len(buf)-1], len(buf))
	} else {
		log.Debugf("\033[33;1m[CS%s|RPCCall|WriteChunk] DataID: %v-%d, Offset: %3d Sec: %v \033[34;1m Data: %v\033[m",
			cs.address[len(cs.address)-1:], args.Handle, args.DataID, args.Offset, args.Secondaries, buf)
	}
	defer func() {
		log.Debugf("\033[33m[CS%s|RPCRet |WriteChunk] DataID: %v-%d, Offset: %3d -> Reply: %+v, Rpcerr: %v\033[m",
			cs.address[len(cs.address)-1:], args.Handle, args.DataID, args.Offset, *reply, rpcerr)
	}()

	defer util.SerializeError(&rpcerr)

	handle := args.Handle
	cinfo, err := cs.getChunk(handle)
	if err != nil {
		return err
	}
	buffer, err := cs.getBuffer(args.DataID)
	if err != nil {
		return err
	}

	cinfo.Lock()
	if !cinfo.isPrimary() {
		cinfo.Unlock()
		return gfs.Error{Code: gfs.NotPrimary, Err: fmt.Sprintf("Chunkserver has no lease of chunk %v", handle)}
	}

	if gfs.Offset(len(buffer))+args.Offset > gfs.MaxChunkSize {
		cinfo.Unlock()
		return gfs.Error{Code: gfs.WriteExceedChunkSize, Err: fmt.Sprintf("Write %d bytes to chunk %v but length is already %d", len(buffer), handle, cinfo.length)}
	}
	offset := args.Offset
	err = cs.applyMutation(cinfo, buffer, offset, false)
	if err != nil {
		cinfo.Unlock()
		return err
	}
	sid := cinfo.currentSN

	if cinfo.leaseExpire.Sub(time.Now()).Seconds()*2 < gfs.LeaseExpire.Seconds() {
		cs.pendingLeaseExtensions.Add(handle)
	}
	cinfo.Unlock()

	err = util.CallAll(args.Secondaries, "ChunkServer.RPCApplyMutation", gfs.ApplyMutationArg{
		Mtype:    gfs.MutationWrite,
		Handle:   handle,
		SerialNo: sid,
		DataID:   args.DataID,
		Offset:   offset,
		Length:   len(buffer)})

	if err != nil {
		return gfs.Error{Code: gfs.DispatchError, Err: fmt.Sprintf("Failed to apply mutation to secondaries: %v", err)}
	}

	cs.dl.Delete(args.DataID)
	return nil
}

// RPCAppendChunk is called by client to apply atomic record append.
// The length of data should be within max append size.
// If the chunk size after appending the data will excceed the limit,
// pad current chunk and ask the client to retry on the next chunk.
func (cs *ChunkServer) RPCAppendChunk(args gfs.AppendChunkArg, reply *gfs.AppendChunkReply) (rpcerr error) {
	log.Debugf("\033[33;1m[CS%s|RPCCall|AppendChunk] Args: %+v\033[m", cs.address[len(cs.address)-1:], args)
	defer func() {
		log.Debugf("\033[33m[CS%s|RPCRet |AppendChunk] Reply: %+v, Rpcerr: %v\033[m", cs.address[len(cs.address)-1:], *reply, rpcerr)
	}()

	defer util.SerializeError(&rpcerr)

	handle := args.Handle
	cinfo, err := cs.getChunk(handle)
	if err != nil {
		return err
	}
	buffer, err := cs.getBuffer(args.DataID)
	if err != nil {
		return err
	}

	cinfo.Lock()
	if !cinfo.isPrimary() {
		cinfo.Unlock()
		return gfs.Error{Code: gfs.NotPrimary, Err: fmt.Sprintf("Chunkserver has no lease of chunk %v", handle)}
	}

	offset := cinfo.length
	if gfs.Offset(len(buffer))+cinfo.length > gfs.MaxChunkSize {
		//XXX
		buffer := make([]byte, gfs.MaxChunkSize-cinfo.length)
		err = cs.applyMutation(cinfo, buffer, offset, false)
		if err != nil {
			cinfo.Unlock()
			return err
		}
		sid := cinfo.currentSN
		cinfo.Unlock()

		err = util.CallAll(args.Secondaries, "ChunkServer.RPCApplyMutation", &gfs.ApplyMutationArg{
			Mtype:    gfs.MutationPad,
			Handle:   handle,
			SerialNo: sid,
			Offset:   offset,
			Length:   int(gfs.MaxChunkSize - offset)})

		if err != nil {
			return gfs.Error{Code: gfs.DispatchError, Err: fmt.Sprintf("Failed to apply mutation to secondaries: %v", err)}
		}
		return gfs.Error{Code: gfs.AppendExceedChunkSize, Err: fmt.Sprintf("Append %d bytes to chunk %v but length is already %d", len(buffer), handle, cinfo.length)}
	}
	err = cs.applyMutation(cinfo, buffer, offset, false)
	if err != nil {
		cinfo.Unlock()
		return err
	}
	sid := cinfo.currentSN

	if cinfo.leaseExpire.Sub(time.Now()).Seconds()*2 < gfs.LeaseExpire.Seconds() {
		cs.pendingLeaseExtensions.Add(handle)
	}
	cinfo.Unlock()

	err = util.CallAll(args.Secondaries, "ChunkServer.RPCApplyMutation", &gfs.ApplyMutationArg{
		Mtype:    gfs.MutationAppend,
		Handle:   handle,
		SerialNo: sid,
		DataID:   args.DataID,
		Offset:   offset,
		Length:   len(buffer)})
	if err != nil {
		return gfs.Error{Code: gfs.DispatchError, Err: fmt.Sprintf("Failed to apply mutation to secondaries: %v", err)}
	}
	reply.Offset = offset

	cs.dl.Delete(args.DataID)
	return nil
}

// RPCApplyMutation is called by primary to apply mutations
func (cs *ChunkServer) RPCApplyMutation(args gfs.ApplyMutationArg, reply *gfs.ApplyMutationReply) (rpcerr error) {
	buf, _ := cs.getBuffer(args.DataID)
	if len(buf) > 20 {
		log.Debugf("\033[33;1m[CS%s|RPCCall|ApplyMutation] Type: %v, SN: %d, DataID: %v-%d, Offset: %d, Len: %d\033[34;1m Data: [%v ... %v] (len=%d)\033[m",
			cs.address[len(cs.address)-1:], args.Mtype, args.SerialNo, args.Handle, args.DataID, args.Offset, args.Length,
			buf[0], buf[len(buf)-1], len(buf))
	} else {
		log.Debugf("\033[33;1m[CS%s|RPCCall|ApplyMutation] Type: %v, SN: %d, DataID: %v-%d, Offset: %d, Len: %d\033[34;1m Data: %v\033[m",
			cs.address[len(cs.address)-1:], args.Mtype, args.SerialNo, args.Handle, args.DataID, args.Offset, args.Length,
			buf)
	}
	defer func() {
		log.Debugf("\033[33m[CS%s|RPCRet |ApplyMutation] Reply: %+v, Rpcerr: %v\033[m", cs.address[len(cs.address)-1:], *reply, rpcerr)
	}()

	defer util.SerializeError(&rpcerr)

	handle := args.Handle
	cinfo, err := cs.getChunk(handle)
	if err != nil {
		return err
	}

	cinfo.Lock()
	//cinfo.LockDebug()
	defer cinfo.Unlock()

	if cinfo.isPrimary() {
		return gfs.Error{Code: gfs.NotSecondary, Err: fmt.Sprintf("Chunkserver %s is not secondary", cs.address)}
	}

	var buffer []byte
	var bufferErr error
	if args.Mtype == gfs.MutationPad {
		//XXX
		buffer = make([]byte, args.Length)
	} else {
		buffer, bufferErr = cs.getBuffer(args.DataID)
	}

	for {
		if cinfo.currentSN >= args.SerialNo {
			return gfs.Error{Code: gfs.MutationExpired, Err: fmt.Sprintf("Mutation %d expired, currect serial is %d", args.SerialNo, cinfo.currentSN)}
		}
		if cinfo.currentSN+1 == args.SerialNo {
			if bufferErr == nil {
				err = cs.applyMutation(cinfo, buffer, args.Offset, true)
			} else {
				cs.applyMutation(cinfo, []byte{}, 0, true)
			}
			cinfo.serialUpdate.Broadcast()
			if bufferErr != nil {
				return bufferErr
			}
			if args.Mtype != gfs.MutationPad {
				cs.dl.Delete(args.DataID)
			}
			return err
		}
		if !cinfo.timer {
			cinfo.timer = true
			go func(oldSerial gfs.ChunkVersion) {
				time.Sleep(time.Second * 10)
				log.Debugf("=== RPC Apply Mutation: Timer wake up, oldSerial = %d", oldSerial)
				cinfo.Lock()
				if cinfo.currentSN == oldSerial {
					log.Debugf("=== RPC Apply Mutation: Timeout, currentSN = %d", cinfo.currentSN)
					//We can update the SN in file later
					cinfo.currentSN++
					cinfo.serialUpdate.Broadcast()
				}
				log.Debugf("=== RPC Apply Mutation: Timer finish, currentSN = %d", cinfo.currentSN)
				cinfo.timer = false
				cinfo.Unlock()
			}(cinfo.currentSN)
		}
		log.Debugf("=== RPC Apply Mutation: Blocked, currentSN = %d, args.SerialNo = %d", cinfo.currentSN, args.SerialNo)
		log.Debugf("=== RPC Apply Mutation NOTE: serialUpdate %v cinfo %p", cinfo.serialUpdate, &cinfo)
		cinfo.serialUpdate.Wait()
	}
}

// RPCSendCopy is called by master, send the whole copy to given address
func (cs *ChunkServer) RPCSendCopy(args gfs.SendCopyArg, reply *gfs.SendCopyReply) (rpcerr error) {
	log.Debugf("\033[33;1m[CS%s|RPCCall|SendCopy] Args: %+v\033[m", cs.address[len(cs.address)-1:], args)
	defer func() {
		log.Debugf("\033[33m[CS%s|RPCRet |SendCopy] Reply: %+v, Rpcerr: %v\033[m", cs.address[len(cs.address)-1:], *reply, rpcerr)
	}()

	defer util.SerializeError(&rpcerr)

	cinfo, err := cs.getChunk(args.Handle)
	if err != nil {
		return err
	}
	cinfo.RLock()
	defer cinfo.RUnlock()

	fchunk, err := os.Open(cs.serverRoot + getChunkFileName(args.Handle))
	if err != nil {
		return gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to open chunk file %v: %v", args.Handle, err)}
	}
	defer fchunk.Close()

	buffer := make([]byte, cinfo.length)
	n, err := fchunk.Read(buffer)
	if gfs.Offset(n) != cinfo.length {
		return gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Chunk %v length mismatch (In record: %d != File: %d)", args.Handle, cinfo.length, n)}
	}

	var tmp gfs.ApplyCopyReply
	err = util.Call(args.Address, "ChunkServer.RPCApplyCopy", gfs.ApplyCopyArg{
		Handle:   args.Handle,
		Data:     buffer,
		Version:  cinfo.version,
		SerialNo: cinfo.currentSN}, &tmp)

	if err != nil {
		gfsErr, ok := err.(gfs.Error)
		if ok {
			return gfsErr
		}
		return gfs.Error{Code: gfs.NetworkError, Err: fmt.Sprintf("Failed to call RPCApplyCopy: %v", err)}
	}

	return nil
}

// RPCApplyCopy is called by another replica
// rewrite the local version to given copy data
func (cs *ChunkServer) RPCApplyCopy(args gfs.ApplyCopyArg, reply *gfs.ApplyCopyReply) (rpcerr error) {
	log.Debugf("\033[33;1m[CS%s|RPCCall|ApplyCopy] Handle: %v\033[m", cs.address[len(cs.address)-1:], args.Handle)
	defer func() {
		log.Debugf("\033[33m[CS%s|RPCRet |ApplyCopy] Reply: %+v, Rpcerr: %v\033[m", cs.address[len(cs.address)-1:], *reply, rpcerr)
	}()

	defer util.SerializeError(&rpcerr)

	var err error
	cs.chunkMutex.RLock()
	cinfo, ok := cs.chunk[args.Handle]
	cs.chunkMutex.RUnlock()
	if !ok {
		cinfo, err = cs.newChunk(args.Handle)
		if err != nil {
			return err
		}
	}
	cinfo.Lock()
	defer cinfo.Unlock()

	//fchunk, err := os.Open(cs.serverRoot + getChunkFileName(args.Handle))
	fchunk, err := os.OpenFile(cs.serverRoot+getChunkFileName(cinfo.handle), os.O_RDWR, 0666)
	if err != nil {
		return gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to open chunk %v: %v", args.Handle, err)}
	}
	defer fchunk.Close()
	err = fchunk.Truncate(int64(len(args.Data)))
	if err != nil {
		return gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to truncate chunk %v: %v", args.Handle, err)}
	}
	_, err = fchunk.WriteAt(args.Data, 0)
	if err != nil {
		return gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to write to chunk %v: %v", args.Handle, err)}
	}
	//TODO: Calculate the checksum
	cinfo.length = gfs.Offset(len(args.Data))
	cinfo.version = args.Version
	cinfo.currentSN = args.SerialNo

	return nil
}

// RPCGrantLease is called by master
// mark the chunkserver as primary
func (cs *ChunkServer) RPCGrantLease(args gfs.GrantLeaseArg, reply *gfs.GrantLeaseReply) (rpcerr error) {
	log.Debugf("\033[33;1m[CS%s|RPCCall|GrantLease] Leases: %+v\033[m", cs.address[len(cs.address)-1:], args.Leases)
	defer func() {
		log.Debugf("\033[33m[CS%s|RPCRet |GrantLease] Reply: %+v, Rpcerr: %v\033[m", cs.address[len(cs.address)-1:], *reply, rpcerr)
	}()

	defer util.SerializeError(&rpcerr)
	reply.Errors = make([]error, len(args.Leases))
	for idx := 0; idx < len(args.Leases); idx++ {
		lease := args.Leases[idx]
		cinfo, err := cs.getChunk(lease.Handle)
		if err != nil {
			reply.Errors[idx] = err
			continue
		}
		cinfo.Lock()
		err = cs.updateVersion(cinfo, lease.NewVersion)
		if err != nil {
			cinfo.Unlock()
			reply.Errors[idx] = err
			continue
		}
		cinfo.leaseExpire = lease.Expire
		cinfo.Unlock()
	}
	return nil
}

// RPCUpdateVersion is called by master
// update the given chunks' version to `NewVersion'
func (cs *ChunkServer) RPCUpdateVersion(args gfs.UpdateVersionArg, reply *gfs.UpdateVersionReply) (rpcerr error) {
	log.Debugf("\033[33;1m[CS%s|RPCCall|UpdateVersion] Chunks: %+v\033[m", cs.address[len(cs.address)-1:], args.Chunks)
	defer func() {
		log.Debugf("\033[33m[CS%s|RPCRet |UpdateVersion] Reply: %+v, Rpcerr: %v\033[m", cs.address[len(cs.address)-1:], *reply, rpcerr)
	}()

	defer util.SerializeError(&rpcerr)
	reply.Errors = make([]error, len(args.Chunks))
	for idx := 0; idx < len(args.Chunks); idx++ {
		update := args.Chunks[idx]
		cinfo, err := cs.getChunk(update.Handle)
		if err != nil {
			reply.Errors[idx] = err
			continue
		}
		cinfo.Lock()
		err = cs.updateVersion(cinfo, update.NewVersion)
		cinfo.Unlock()
		if err != nil {
			reply.Errors[idx] = err
		}
	}
	return nil
}

// RPCGetChunkList is called by master
// request the chunkserver to report the chunks it has
func (cs *ChunkServer) RPCGetChunkList(args gfs.GetChunkListArg, reply *gfs.GetChunkListReply) (rpcerr error) {
	log.Debugf("\033[33;1m[CS%s|RPCCall|GetChunkList]\033[m", cs.address[len(cs.address)-1:])
	defer func() {
		log.Debugf("\033[33m[CS%s|RPCRet |GetChunkList] Reply: %+v, Rpcerr: %v\033[m", cs.address[len(cs.address)-1:], *reply, rpcerr)
	}()

	defer util.SerializeError(&rpcerr)
	cs.chunkMutex.RLock()
	defer cs.chunkMutex.RUnlock()
	for _, v := range cs.chunk {
		//v.RLock()
		//v.RLockDebug()
		if v.lastReport { //Golang RWMutex seems to be weird
			reply.Chunks = append(reply.Chunks, gfs.ChunkReportType{Handle: v.handle, Version: v.version})
		} else {
			v.RLock()
			reply.Chunks = append(reply.Chunks, gfs.ChunkReportType{Handle: v.handle, Version: v.version})
			v.RUnlock()
		}
		//v.RUnlock()
	}
	return nil
}

// RPCGetBufferAddress is called by client or other chunkservers
// get the buffer address (for data connection) of the chunkserver
func (cs *ChunkServer) RPCGetBufferAddress(args gfs.GetBufferAddressArg, reply *gfs.GetBufferAddressReply) (rpcerr error) {
	defer util.SerializeError(&rpcerr)
	reply.Address = cs.dlAddr
	return nil
}

func (cs *ChunkServer) newChunk(handle gfs.ChunkHandle) (*chunkInfo, error) {
	fname, finfoname := cs.serverRoot+getChunkFileName(handle), cs.serverRoot+getChunkInfoFileName(handle)
	finfo, err := os.Create(finfoname)
	if err != nil {
		return nil, gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to create chunk  %v's info file: %v", handle, err)}
	}
	defer finfo.Close()

	_, err = finfo.Write(magicChunkInfoFile)
	if err != nil {
		return nil, gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to write chunk %v's info file: %v", handle, err)}
	}
	err = binary.Write(finfo, binary.LittleEndian, int64(0))
	if err != nil {
		return nil, gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to write chunk %v's info file: %v", handle, err)}
	}
	err = binary.Write(finfo, binary.LittleEndian, int64(0))
	if err != nil {
		return nil, gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to write chunk %v's info file: %v", handle, err)}
	}

	fchunk, err := os.Create(fname)
	if err != nil {
		return nil, gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to create chunk %v: %v", handle, err)}
	}
	defer fchunk.Close()

	cs.chunkMutex.Lock()
	defer cs.chunkMutex.Unlock()
	cs.chunk[handle] = &chunkInfo{
		cs:        cs,
		handle:    handle,
		length:    0,
		version:   0,
		currentSN: 0,
		timer:     false}
	cs.chunk[handle].serialUpdate = sync.NewCond(cs.chunk[handle])

	return cs.chunk[handle], nil
}

func (cs *ChunkServer) applyMutation(cinfo *chunkInfo, data []byte, offset gfs.Offset, forceUpdateSN bool) (reterr error) {
	defer cs.reportFileSystemErr(cinfo.handle, &reterr)
	deferUpdateSN := forceUpdateSN
	defer func(newSN gfs.ChunkVersion) {
		if !deferUpdateSN {
			return
		}
		log.Debugf(">>> applyMutation: Defer update sn (err=%v)", reterr)
		cinfo.currentSN = newSN
		//finfo, err := os.Open(cs.serverRoot + getChunkInfoFileName(cinfo.handle))
		finfo, err := os.OpenFile(cs.serverRoot+getChunkInfoFileName(cinfo.handle), os.O_RDWR, 0666)
		if err != nil {
			return
		}
		defer finfo.Close()
		buffer := bytes.NewBuffer(make([]byte, 8))
		binary.Write(buffer, binary.LittleEndian, newSN)
		finfo.WriteAt(buffer.Bytes(), 16)
	}(cinfo.currentSN + 1)

	if len(data) != 0 || offset != 0 {
		log.Debugf(">>> applyMutation: Writing Chunk...")
		//fchunk, err := os.Open(cs.serverRoot + getChunkFileName(cinfo.handle))
		fchunk, err := os.OpenFile(cs.serverRoot+getChunkFileName(cinfo.handle), os.O_RDWR, 0666)
		if err != nil {
			return gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to open chunk %v: %v", cinfo.handle, err)}
		}
		defer fchunk.Close()

		n, err := fchunk.WriteAt(data, int64(offset))
		if err != nil {
			return gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to write chunk %v: %v", cinfo.handle, err)}
		}

		if offset+gfs.Offset(n) > cinfo.length {
			cinfo.length = offset + gfs.Offset(n)
		}
	}

	log.Debugf(">>> applyMutation: Writing Chunk Info...")

	//finfo, err := os.Open(cs.serverRoot + getChunkInfoFileName(cinfo.handle))
	finfo, err := os.OpenFile(cs.serverRoot+getChunkInfoFileName(cinfo.handle), os.O_RDWR, 0666)
	if err != nil {
		return gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to open chunk %v's info file: %v", cinfo.handle, err)}
	}
	defer finfo.Close()
	//TODO: Write checksum
	buffer := new(bytes.Buffer)
	binary.Write(buffer, binary.LittleEndian, cinfo.currentSN+1)
	_, err = finfo.WriteAt(buffer.Bytes(), 16)
	if err != nil {
		return gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to update serial number of chunk %v: %v", cinfo.handle, err)}
	}
	cinfo.currentSN++
	deferUpdateSN = false

	log.Debugf(">>> applyMutation: Done!")
	return nil
}

func (cs *ChunkServer) updateVersion(cinfo *chunkInfo, newVersion gfs.ChunkVersion) (reterr error) {
	defer cs.reportFileSystemErr(cinfo.handle, &reterr)
	if cinfo.version >= newVersion {
		return gfs.Error{Code: gfs.VersionNewer, Err: fmt.Sprintf("Chunk %v's version (%d) is newer than request (%d)", cinfo.handle, cinfo.version, newVersion)}
	}
	if cinfo.version < newVersion-1 {
		return gfs.Error{Code: gfs.VersionOlder, Err: fmt.Sprintf("Chunk %v out of date (version: %d)", cinfo.handle, cinfo.version)}
	}
	//finfo, err := os.Open(cs.serverRoot + getChunkInfoFileName(cinfo.handle))
	finfo, err := os.OpenFile(cs.serverRoot+getChunkInfoFileName(cinfo.handle), os.O_RDWR, 0666)
	if err != nil {
		return gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to open chunk %v's info file: %v", cinfo.handle, err)}
	}
	defer finfo.Close()
	buffer := new(bytes.Buffer)
	binary.Write(buffer, binary.LittleEndian, newVersion)
	_, err = finfo.WriteAt(buffer.Bytes(), 8)
	if err != nil {
		return gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to update version of chunk %v: %v", cinfo.handle, err)}
	}
	cinfo.version = newVersion
	return nil
}

func (cs *ChunkServer) getChunk(handle gfs.ChunkHandle) (*chunkInfo, error) {
	cs.chunkMutex.RLock()
	cinfo, ok := cs.chunk[handle]
	cs.chunkMutex.RUnlock()
	if !ok {
		cs.failedChunks.Add(handle)
		return nil, gfs.Error{Code: gfs.ChunkNotFound, Err: fmt.Sprintf("Cannot find chunk %v", handle)}
	}
	return cinfo, nil
}

func (cs *ChunkServer) getBuffer(dataID gfs.BufferHandle) ([]byte, error) {
	buffer, ok := cs.dl.Get(dataID)
	if !ok {
		return nil, gfs.Error{Code: gfs.BufferNotFound, Err: fmt.Sprintf("Cannot find buffer %d", dataID)}
	}
	return buffer, nil
}

func (cs *ChunkServer) reportFileSystemErr(handle gfs.ChunkHandle, err *error) {
	if *err == nil {
		return
	}
	if (*err).(gfs.Error).Code == gfs.FileSystemError {
		cs.failedChunks.Add(handle)
	}
	return
}

func (cs *ChunkServer) loadChunks() error {
	files, err := ioutil.ReadDir(cs.serverRoot)
	if err != nil {
		return gfs.Error{Code: gfs.FileSystemError, Err: fmt.Sprintf("Failed to list files in server root: %v", err)}
	}
	regInfo := regexp.MustCompile(`[0-9|A-F|a-f]{16}\.info`)
	filelist := make(map[string]int64)
	for _, v := range files {
		if !v.IsDir() {
			filelist[v.Name()] = v.Size()
		}
	}

loadChunkFiles:
	for k := range filelist {
		if regInfo.MatchString(k) {
			len, ok := filelist[k[:16]+".chunk"]
			if !ok {
				continue
			}

			finfo, err := os.Open(cs.serverRoot + k)
			if err != nil {
				continue
			}
			tmp := make([]byte, 8)
			_, err = finfo.ReadAt(tmp, 0)
			if err != nil {
				finfo.Close()
				continue
			}
			for i := 0; i < 8; i++ {
				if tmp[i] != magicChunkInfoFile[i] {
					finfo.Close()
					continue loadChunkFiles
				}
			}
			tmp = make([]byte, 16)
			_, err = finfo.ReadAt(tmp, 8)
			finfo.Close()
			if err != nil {
				continue
			}
			buffer := bytes.NewBuffer(tmp)
			var version, serialNo gfs.ChunkVersion
			binary.Read(buffer, binary.LittleEndian, &version)
			binary.Read(buffer, binary.LittleEndian, &serialNo)
			var handle gfs.ChunkHandle
			fmt.Sscanf(k[:16], "%x", &handle)
			cs.chunk[handle] = &chunkInfo{
				cs:        cs,
				handle:    handle,
				length:    gfs.Offset(len),
				version:   version,
				currentSN: serialNo,
				timer:     false}
			cs.chunk[handle].serialUpdate = sync.NewCond(cs.chunk[handle])
		}
	}
	return nil
}
