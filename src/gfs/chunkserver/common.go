package chunkserver

import (
	"fmt"
	"gfs"
)

const sizeChunkInfoFileHeader = 16 //sizeof gfs.ChunkVersion
var magicChunkInfoFile = []byte{'C', 'H', 'U', 'N', 'K', 0x7F, 0, 0}

func getChunkFileName(handle gfs.ChunkHandle) string {
	return fmt.Sprintf("%v.chunk", handle)
}

func getChunkInfoFileName(handle gfs.ChunkHandle) string {
	return fmt.Sprintf("%v.info", handle)
}
